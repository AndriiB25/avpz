﻿using System;
using System.Globalization;
using System.Windows.Controls;

namespace AVPZ.Lab5.Validation
{
    public class DoubleValidationRule : ValidationRule
    {
        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            if (!double.TryParse(Convert.ToString(value), NumberStyles.Number, CultureInfo.InvariantCulture, out _))
            {
                return new ValidationResult(false, "Це повинна бути цифра");
            }
            else
            {
                return ValidationResult.ValidResult;
            }
        }
    }
}
