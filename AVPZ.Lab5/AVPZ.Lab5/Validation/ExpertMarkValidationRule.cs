﻿using System;
using System.Globalization;
using System.Windows.Controls;

namespace AVPZ.Lab5.Validation
{
    public class ExpertMarkValidationRule : ValidationRule
    {
        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            if (!double.TryParse(Convert.ToString(value), NumberStyles.Number, CultureInfo.InvariantCulture, out double number))
            {
                return new ValidationResult(false, "Це повинна бути цифра");
            }
            else if (number < 0 || number > 1)
            {
                return new ValidationResult(false, "Оцінка повинна бути лише в межах від 0 до 1");
            }
            else
            {
                return ValidationResult.ValidResult;
            }
        }
    }
}
