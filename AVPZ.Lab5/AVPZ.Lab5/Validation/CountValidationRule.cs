﻿using System;
using System.Globalization;
using System.Windows.Controls;

namespace AVPZ.Lab5.Validation
{
    public class CountValidationRule : ValidationRule
    {
        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            if (!int.TryParse(Convert.ToString(value), NumberStyles.Number, CultureInfo.InvariantCulture, out int num))
            {
                return new ValidationResult(false, "Це повинна бути цифра");
            }
            else if (num != 0 && num != 1)
            {
                return new ValidationResult(false, "Кількість повинна бути 1 або 0");
            }
            else
            {
                return ValidationResult.ValidResult;
            }
        }
    }
}
