﻿#pragma checksum "..\..\..\MainWindow.xaml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "FD1F3C551FD2EE0CC1E6104EC3302AA5B83133F7"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using AVPZ.Lab5;
using AVPZ.Lab5.Converters;
using AVPZ.Lab5.Validation;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Controls.Ribbon;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace AVPZ.Lab5 {
    
    
    /// <summary>
    /// MainWindow
    /// </summary>
    public partial class MainWindow : System.Windows.Window, System.Windows.Markup.IComponentConnector {
        
        
        #line 48 "..\..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGrid SourcesOfRisksTable;
        
        #line default
        #line hidden
        
        
        #line 101 "..\..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGrid RiskEventsTable;
        
        #line default
        #line hidden
        
        
        #line 161 "..\..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGrid TechEventsProbTable;
        
        #line default
        #line hidden
        
        
        #line 559 "..\..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGrid ValuedEventsProbTable;
        
        #line default
        #line hidden
        
        
        #line 947 "..\..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGrid PlannedEventsProbTable;
        
        #line default
        #line hidden
        
        
        #line 1337 "..\..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGrid RealisationEventsProbTable;
        
        #line default
        #line hidden
        
        
        #line 1728 "..\..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGrid TechEventLosesTable;
        
        #line default
        #line hidden
        
        
        #line 2109 "..\..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGrid ValuedEventLosesTable;
        
        #line default
        #line hidden
        
        
        #line 2490 "..\..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGrid PlannedEventLosesTable;
        
        #line default
        #line hidden
        
        
        #line 2874 "..\..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGrid RealisationEventLosesTable;
        
        #line default
        #line hidden
        
        
        #line 3254 "..\..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGrid ProjectRealisationPricesTable;
        
        #line default
        #line hidden
        
        
        #line 3309 "..\..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGrid EventsToReduceRisksTable;
        
        #line default
        #line hidden
        
        
        #line 3357 "..\..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox AcceptanceTextBox;
        
        #line default
        #line hidden
        
        
        #line 3370 "..\..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox AvoidanceTextBox;
        
        #line default
        #line hidden
        
        
        #line 3378 "..\..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button SaveEventToReduceRiskButton;
        
        #line default
        #line hidden
        
        
        #line 3395 "..\..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGrid MonitoringOfRisksTable;
        
        #line default
        #line hidden
        
        
        #line 3442 "..\..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label FirstIntervalLabel;
        
        #line default
        #line hidden
        
        
        #line 3447 "..\..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label SecondIntervalLabel;
        
        #line default
        #line hidden
        
        
        #line 3452 "..\..\..\MainWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label ThirdIntervalLabel;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "7.0.11.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/AVPZ.Lab5;component/mainwindow.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\MainWindow.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "7.0.11.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.SourcesOfRisksTable = ((System.Windows.Controls.DataGrid)(target));
            return;
            case 2:
            this.RiskEventsTable = ((System.Windows.Controls.DataGrid)(target));
            return;
            case 3:
            this.TechEventsProbTable = ((System.Windows.Controls.DataGrid)(target));
            return;
            case 4:
            this.ValuedEventsProbTable = ((System.Windows.Controls.DataGrid)(target));
            return;
            case 5:
            this.PlannedEventsProbTable = ((System.Windows.Controls.DataGrid)(target));
            return;
            case 6:
            this.RealisationEventsProbTable = ((System.Windows.Controls.DataGrid)(target));
            return;
            case 7:
            this.TechEventLosesTable = ((System.Windows.Controls.DataGrid)(target));
            return;
            case 8:
            this.ValuedEventLosesTable = ((System.Windows.Controls.DataGrid)(target));
            return;
            case 9:
            this.PlannedEventLosesTable = ((System.Windows.Controls.DataGrid)(target));
            return;
            case 10:
            this.RealisationEventLosesTable = ((System.Windows.Controls.DataGrid)(target));
            return;
            case 11:
            this.ProjectRealisationPricesTable = ((System.Windows.Controls.DataGrid)(target));
            return;
            case 12:
            this.EventsToReduceRisksTable = ((System.Windows.Controls.DataGrid)(target));
            
            #line 3321 "..\..\..\MainWindow.xaml"
            this.EventsToReduceRisksTable.SelectionChanged += new System.Windows.Controls.SelectionChangedEventHandler(this.EventsToReduceRisksTable_SelectionChanged);
            
            #line default
            #line hidden
            return;
            case 13:
            this.AcceptanceTextBox = ((System.Windows.Controls.TextBox)(target));
            
            #line 3363 "..\..\..\MainWindow.xaml"
            this.AcceptanceTextBox.PreviewTextInput += new System.Windows.Input.TextCompositionEventHandler(this.AvoidanceTextBox_PreviewTextInput);
            
            #line default
            #line hidden
            return;
            case 14:
            this.AvoidanceTextBox = ((System.Windows.Controls.TextBox)(target));
            
            #line 3376 "..\..\..\MainWindow.xaml"
            this.AvoidanceTextBox.PreviewTextInput += new System.Windows.Input.TextCompositionEventHandler(this.AvoidanceTextBox_PreviewTextInput);
            
            #line default
            #line hidden
            return;
            case 15:
            this.SaveEventToReduceRiskButton = ((System.Windows.Controls.Button)(target));
            
            #line 3382 "..\..\..\MainWindow.xaml"
            this.SaveEventToReduceRiskButton.Click += new System.Windows.RoutedEventHandler(this.SaveEventToReduceRiskButton_Click);
            
            #line default
            #line hidden
            return;
            case 16:
            this.MonitoringOfRisksTable = ((System.Windows.Controls.DataGrid)(target));
            return;
            case 17:
            this.FirstIntervalLabel = ((System.Windows.Controls.Label)(target));
            return;
            case 18:
            this.SecondIntervalLabel = ((System.Windows.Controls.Label)(target));
            return;
            case 19:
            this.ThirdIntervalLabel = ((System.Windows.Controls.Label)(target));
            return;
            }
            this._contentLoaded = true;
        }
    }
}

