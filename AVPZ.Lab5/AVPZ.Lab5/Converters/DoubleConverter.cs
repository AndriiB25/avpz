﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace AVPZ.Lab5.Converters
{
    public class DoubleConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is double doubleValue)
            {
                return doubleValue.ToString("#.##", CultureInfo.InvariantCulture).Replace('.', ',');
            }

            return value;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            string? strValue = System.Convert.ToString(value);
            double resultDouble;
            if (double.TryParse(strValue, NumberStyles.Number, CultureInfo.InvariantCulture, out resultDouble))
            {
                return resultDouble;
            }

            return DependencyProperty.UnsetValue;
        }
    }
}
