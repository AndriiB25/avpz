﻿using AVPZ.Lab5.DataGenerators;
using AVPZ.Lab5.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;

namespace AVPZ.Lab5
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        #region Constants
        const int TechRisksTotal = 12, PriceRisksTotal = 8,
            PlannedRisksTotal = 10, ProjectRealisationRisksTotal = 15, ReducedForProb = 12, ReducedForLosses = 11;
        const double C211 = 0.1, C212 = 0.05, C213 = 0.12, C214 = 0.14, 
                    C221 = 0.11, C222 = 0.07, C223 = 0.09, C224 = 0.14;
        #endregion

        #region Binding collections
        #region Helping
        private List<double> _startPrices { get; set; } = new();
        private List<double> _finalPrices = new();
        private List<(double Start, double End)> _intervals = new();
        #endregion
        #region Sources of risks
        public ObservableCollection<SourceOfRisk> SourcesOfRisks { get; set; } = new();
        #endregion
        #region Potential risk events
        public ObservableCollection<SourceOfRisk> PotentialRiskEvents { get; set; } = new();
        #endregion
        #region Probs
        public ObservableCollection<ProbabilityOfRiskEvent> TechProbs { get; set; } = new();
        public ObservableCollection<ProbabilityOfRiskEvent> PriceProbs { get; set; } = new();
        public ObservableCollection<ProbabilityOfRiskEvent> PlannedProbs { get; set; } = new();
        public ObservableCollection<ProbabilityOfRiskEvent> RealisationProbs { get; set; } = new();
        #endregion
        #region Losses
        public ObservableCollection<LosesFromRisk> TechLosses { get; set; } = new();
        public ObservableCollection<LosesFromRisk> PriceLosses { get; set; } = new();
        public ObservableCollection<LosesFromRisk> PlannedLosses { get; set; } = new();
        public ObservableCollection<LosesFromRisk> RealisationLosses { get; set; } = new();
        #endregion
        #region Project realisation prices
        public ObservableCollection<ProjectRealisationPrice> ProjectRealisationPrices { get; set; } = new();
        #endregion
        #region Events to reduce risks
        public ObservableCollection<EventToReduceRisk> EventsToReduceRisks { get; set; } = new();
        #endregion
        #region Monitoring data
        public ObservableCollection<MonitoringOfRisk> MonitoringOfRisks { get; set; } = new();
        #endregion
        #endregion


        public MainWindow()
        {
            InitializeComponent();

            var sourcesOfRisks = TablesDataGenerator.CreateSourcesOfRisks(); // 1.1
            sourcesOfRisks.ForEach(SourcesOfRisks.Add);

            var setOfRisks = TablesDataGenerator.CreatePotentialRiskedEvents(); // 1.2
            setOfRisks.ForEach(PotentialRiskEvents.Add);

            SetUpDataForProbabilityTables(); // 2.1
            
            SetUpDataForLossesTables(); // 2.2

            var eventsToReduceRisks = TablesDataGenerator.CreateEventsToReduceRisks(); // 3.1
            eventsToReduceRisks.ForEach(EventsToReduceRisks.Add);
            
            SetupDataForMonitoringTable(); // 4.1

            SubscribeToUpdateEvents();
        }

        #region UI Events
        private void AvoidanceTextBox_PreviewTextInput(object sender, System.Windows.Input.TextCompositionEventArgs e)
        {
            // Check if the entered text is a valid digit (0 or 1)
            if (!IsTextAllowed(e.Text))
            {
                e.Handled = true; // If not allowed, mark the event as handled
            }
        }

        private bool IsTextAllowed(string text)
        {
            return Regex.IsMatch(text, "[01]");
        }

        private void SaveEventToReduceRiskButton_Click(object sender, RoutedEventArgs e)
        {
            var acceptance = int.Parse(AcceptanceTextBox.Text);
            var avoidance = int.Parse(AvoidanceTextBox.Text);
            if(avoidance == acceptance)
            {
                MessageBox.Show("Значення не можуть бути однаковими!", "Увага!", MessageBoxButton.OKCancel, MessageBoxImage.Warning);
            }
            else
            {
                var selectedEvent = EventsToReduceRisksTable.SelectedItem as EventToReduceRisk;
                if (selectedEvent != null)
                {
                    var el = EventsToReduceRisks.First(x => x.Name == selectedEvent.Name);
                    el.Avoidance = avoidance;
                    el.Acceptance = acceptance;
                    AvoidanceTextBox.IsEnabled = false;
                    AcceptanceTextBox.IsEnabled = false;
                }
            }
        }

        private void EventsToReduceRisksTable_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var selectedEvent = EventsToReduceRisksTable.SelectedItem as EventToReduceRisk;
            if (selectedEvent != null)
            {
                AcceptanceTextBox.Text = selectedEvent.Acceptance.ToString();
                AvoidanceTextBox.Text = selectedEvent.Avoidance.ToString();
                AvoidanceTextBox.IsEnabled = true;
                AcceptanceTextBox.IsEnabled = true;
            }
        }
        #endregion

        #region Populating tables
        // 2.1
        private void SetUpDataForProbabilityTables()
        {
            var risks = SplitRisks();

            var coeffs = TablesDataGenerator.GenerateWeightingCoefficientsOfTheExperts(ReducedForProb);

            var tech = TablesDataGenerator.GenerateProbabilityOfOccurrenceOfRiskEvents(risks.Tech, TechRisksTotal, coeffs.Tech, C211).ToList();
            tech.ForEach(e => TechProbs.Add(e));
            
            var price = TablesDataGenerator.GenerateProbabilityOfOccurrenceOfRiskEvents(risks.Price, PriceRisksTotal, coeffs.Valued, C212).ToList();
            price.ForEach(e => PriceProbs.Add(e));

            var planned = TablesDataGenerator.GenerateProbabilityOfOccurrenceOfRiskEvents(risks.Planned, PlannedRisksTotal, coeffs.Planned, C213).ToList();
            planned.ForEach(e => PlannedProbs.Add(e));

            var realisation = TablesDataGenerator.GenerateProbabilityOfOccurrenceOfRiskEvents(risks.Realisation, ProjectRealisationRisksTotal, coeffs.Realisation, C214).ToList();
            realisation.ForEach(e => RealisationProbs.Add(e));
        }

        // 2.2
        private void SetUpDataForLossesTables()
        {
            var risks = SplitRisks();

            var coeffs = TablesDataGenerator.GenerateWeightingCoefficientsOfTheExperts(ReducedForLosses);
            _startPrices = TablesDataGenerator.GenerateProjectRealisationPrices().ToList();

            var tech = TablesDataGenerator.GeneratePossibleLossesFromRisk(risks.Tech, TechRisksTotal, coeffs.Tech, C221, _startPrices[0]);
            tech.Data.ForEach(TechLosses.Add);
            
            var price = TablesDataGenerator.GeneratePossibleLossesFromRisk(risks.Price, PriceRisksTotal, coeffs.Valued, C222, _startPrices[1]);
            price.Data.ForEach(PriceLosses.Add);
            
            var planned = TablesDataGenerator.GeneratePossibleLossesFromRisk(risks.Planned, PlannedRisksTotal, coeffs.Planned, C223, _startPrices[2]);
            planned.Data.ForEach(PlannedLosses.Add);

            var realisation = TablesDataGenerator.GeneratePossibleLossesFromRisk(risks.Realisation, ProjectRealisationRisksTotal, coeffs.Realisation, C223, _startPrices[3]);
            realisation.Data.ForEach(RealisationLosses.Add);

            // _losesOfRiskEvents.Add(tech.Data); _losesOfRiskEvents.Add(price.Data); _losesOfRiskEvents.Add(planned.Data); _losesOfRiskEvents.Add(realisation.Data);
            _finalPrices.Add(tech.FinalPrice); _finalPrices.Add(price.FinalPrice); _finalPrices.Add(planned.FinalPrice); _finalPrices.Add(realisation.FinalPrice);
            var prices = TablesDataGenerator.GenerateProjectRealisationPricesData(_startPrices, _finalPrices);
            prices.ForEach(ProjectRealisationPrices.Add);
        }

        // 4.1
        private void SetupDataForMonitoringTable()
        {
            var techData = TechLosses.Select(x => (x.EventName, x.Count, x.AdditionalPrice)).ToList();
            var priceData = PriceLosses.Select(x => (x.EventName, x.Count, x.AdditionalPrice)).ToList();
            var plannedData = PlannedLosses.Select(x => (x.EventName, x.Count, x.AdditionalPrice)).ToList();
            var realisationData = RealisationLosses.Select(x => (x.EventName, x.Count, x.AdditionalPrice)).ToList();

            var result = TablesDataGenerator.GenerateMonitoringData(techData, priceData, plannedData, realisationData);
            
            result.MonitoringData.ForEach(MonitoringOfRisks.Add);
            _intervals.AddRange(result.Intervals);
            
            DisplayIntervals();
        }
        
        private void DisplayIntervals()
        {
            FirstIntervalLabel.Content = $"Низький: {_intervals[0].Start} - {_intervals[0].End}";
            SecondIntervalLabel.Content = $"Середній: {_intervals[1].Start} - {_intervals[1].End}";
            ThirdIntervalLabel.Content = $"Високий: {_intervals[2].Start} - {_intervals[2].End}";
        }
        
        private (List<SourceOfRisk> Tech, List<SourceOfRisk> Price, List<SourceOfRisk> Planned, List<SourceOfRisk> Realisation) SplitRisks()
        {
            return (PotentialRiskEvents.Take(TechRisksTotal).ToList(), PotentialRiskEvents.Skip(TechRisksTotal).Take(PriceRisksTotal).ToList(),
                PotentialRiskEvents.Skip(TechRisksTotal + PriceRisksTotal).Take(PlannedRisksTotal).ToList(),
                PotentialRiskEvents.Skip(TechRisksTotal + PriceRisksTotal + PlannedRisksTotal).Take(ProjectRealisationRisksTotal).ToList());
        }
        #endregion

        #region Event triggers to recalculate tables' data
        private void SubscribeToUpdateEvents()
        {
            #region Sources of risks and potential risks
            foreach (var s in SourcesOfRisks)
            {
                s.ModifiedFromGrid = true;
                s.CountChanged += SourceOfRisk_CountChanged;
            }

            foreach (var p in PotentialRiskEvents)
            {
                p.ModifiedFromGrid = true;
                p.CountChanged += PotentialRiskEvents_CountChanged;
            }
            #endregion

            #region Probabilities
            foreach (var pr in TechProbs)
            {
                pr.PropertyChanged += TechProbs_PropertyChanged;
            }

            foreach(var pr in PriceProbs)
            {
                pr.PropertyChanged += PriceProbs_PropertyChanged;
            }
            
            foreach(var pr in PlannedProbs)
            {
                pr.PropertyChanged += PlannedProbs_PropertyChanged;
            }

            foreach (var pr in RealisationProbs)
            {
                pr.PropertyChanged += RealisationProbs_PropertyChanged;
            }
            #endregion

            #region Losses
            foreach(var l in TechLosses)
            {
                l.PropertyChanged += TechLosses_PropertyChanged;
            }

            foreach (var l in PriceLosses)
            {
                l.PropertyChanged += PriceLosses_PropertyChanged;
            }

            foreach (var l in PlannedLosses)
            {
                l.PropertyChanged += PlannedLosses_PropertyChanged;
            }

            foreach (var l in RealisationLosses)
            {
                l.PropertyChanged += RealisationLosses_PropertyChanged; ;
            }
            #endregion
        }

        private void SourceOfRisk_CountChanged(object sender, EventArgs e)
        {
            #region Unsubscribe
            
            // це забезпечує уникнення stack overflow
            foreach (var s in SourcesOfRisks)
            {
                //s.ModifiedFromGrid = true;
                s.CountChanged -= SourceOfRisk_CountChanged;
            }
            #endregion

            DataRecalculation.RecalculateSourcesOfRisks(SourcesOfRisks);

            #region Subscribe again
            foreach (var s in SourcesOfRisks)
            {
                //s.ModifiedFromGrid = true;
                s.CountChanged += SourceOfRisk_CountChanged;
            }
            #endregion
        }

        private void PotentialRiskEvents_CountChanged(object? sender, EventArgs e)
        {
            #region Unsubscribe

            // це забезпечує уникнення stack overflow
            foreach (var p in PotentialRiskEvents)
            {
                //p.ModifiedFromGrid = true;
                p.CountChanged -= PotentialRiskEvents_CountChanged;
            }
            #endregion

            DataRecalculation.RecalculatePotentialRisks(PotentialRiskEvents, TechProbs, PriceProbs, PlannedProbs, RealisationProbs, 
                TechLosses, PriceLosses, PlannedLosses, RealisationLosses, MonitoringOfRisks);
            DataRecalculation.RecalculateMonitoringData(MonitoringOfRisks, _intervals);
            DisplayIntervals();

            #region Subscribe again
            foreach (var p in PotentialRiskEvents)
            {
                //p.ModifiedFromGrid = false;
                p.CountChanged += PotentialRiskEvents_CountChanged;
            }
            #endregion
        }

        private void TechProbs_PropertyChanged(object? sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            #region Unsubscribe

            // це забезпечує уникнення stack overflow
            foreach (var pr in TechProbs)
            {
                //s.ModifiedFromGrid = true;
                pr.PropertyChanged -= TechProbs_PropertyChanged;
            }
            #endregion

            DataRecalculation.RecalculateRiskProbability(TechProbs);

            #region Subscribe again
            foreach (var pr in TechProbs)
            {
                //s.ModifiedFromGrid = true;
                pr.PropertyChanged += TechProbs_PropertyChanged;
            }
            #endregion
        }

        private void PriceProbs_PropertyChanged(object? sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            #region Unsubscribe

            // це забезпечує уникнення stack overflow
            foreach (var pr in PriceProbs)
            {
                //s.ModifiedFromGrid = true;
                pr.PropertyChanged -= PriceProbs_PropertyChanged;
            }
            #endregion

            DataRecalculation.RecalculateRiskProbability(PriceProbs);

            #region Subscribe again
            foreach (var pr in PriceProbs)
            {
                //s.ModifiedFromGrid = true;
                pr.PropertyChanged += PriceProbs_PropertyChanged;
            }
            #endregion
        }

        private void PlannedProbs_PropertyChanged(object? sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            #region Unsubscribe

            // це забезпечує уникнення stack overflow
            foreach (var pr in PlannedProbs)
            {
                //s.ModifiedFromGrid = true;
                pr.PropertyChanged -= PlannedProbs_PropertyChanged;
            }
            #endregion

            DataRecalculation.RecalculateRiskProbability(PlannedProbs);

            #region Subscribe again
            foreach (var pr in PlannedProbs)
            {
                //s.ModifiedFromGrid = true;
                pr.PropertyChanged += PlannedProbs_PropertyChanged;
            }
            #endregion
        }

        private void RealisationProbs_PropertyChanged(object? sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            #region Unsubscribe

            // це забезпечує уникнення stack overflow
            foreach (var pr in RealisationProbs)
            {
                //s.ModifiedFromGrid = true;
                pr.PropertyChanged -= RealisationProbs_PropertyChanged;
            }
            #endregion

            DataRecalculation.RecalculateRiskProbability(RealisationProbs);

            #region Subscribe again
            foreach (var pr in RealisationProbs)
            {
                //s.ModifiedFromGrid = true;
                pr.PropertyChanged += RealisationProbs_PropertyChanged;
            }
            #endregion
        }

        private void TechLosses_PropertyChanged(object? sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            #region Unsubscribe
            foreach (var l in TechLosses)
            {
                l.PropertyChanged -= TechLosses_PropertyChanged;
            }
            #endregion

            DataRecalculation.RecalculateRiskLosses(TechLosses, ProjectRealisationPrices, priceIndex: 1, MonitoringOfRisks,
                copyStart: 0, copyEnd: TechRisksTotal);
            DataRecalculation.RecalculateMonitoringData(MonitoringOfRisks, _intervals);
            DisplayIntervals();

            #region Subscribe again
            foreach (var l in TechLosses)
            {
                l.PropertyChanged += TechLosses_PropertyChanged;
            }
            #endregion
        }

        private void PriceLosses_PropertyChanged(object? sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            #region Unsubscribe
            foreach (var l in PriceLosses)
            {
                l.PropertyChanged -= PriceLosses_PropertyChanged;
            }
            #endregion

            DataRecalculation.RecalculateRiskLosses(PriceLosses, ProjectRealisationPrices, priceIndex: 2, MonitoringOfRisks,
                copyStart: TechRisksTotal, copyEnd: TechRisksTotal + PriceRisksTotal);
            DataRecalculation.RecalculateMonitoringData(MonitoringOfRisks, _intervals);
            DisplayIntervals();

            #region Subscribe again
            foreach (var l in PriceLosses)
            {
                l.PropertyChanged += PriceLosses_PropertyChanged;
            }
            #endregion
        }

        private void PlannedLosses_PropertyChanged(object? sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            #region Unsubscribe
            foreach (var l in PlannedLosses)
            {
                l.PropertyChanged -= PlannedLosses_PropertyChanged;
            }
            #endregion

            DataRecalculation.RecalculateRiskLosses(PlannedLosses, ProjectRealisationPrices, priceIndex: 3, MonitoringOfRisks,
                copyStart: TechRisksTotal + PriceRisksTotal, copyEnd: TechRisksTotal + PriceRisksTotal + PlannedRisksTotal);
            DataRecalculation.RecalculateMonitoringData(MonitoringOfRisks, _intervals);
            DisplayIntervals();

            #region Subscribe again
            foreach (var l in PlannedLosses)
            {
                l.PropertyChanged += PriceLosses_PropertyChanged;
            }
            #endregion
        }

        private void RealisationLosses_PropertyChanged(object? sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            #region Unsubscribe
            foreach (var l in RealisationLosses)
            {
                l.PropertyChanged -= RealisationLosses_PropertyChanged;
            }
            #endregion

            DataRecalculation.RecalculateRiskLosses(RealisationLosses, ProjectRealisationPrices, priceIndex: 4, MonitoringOfRisks,
                copyStart: TechRisksTotal + PriceRisksTotal + PlannedRisksTotal, copyEnd: TechRisksTotal + PriceRisksTotal + PlannedRisksTotal + ProjectRealisationRisksTotal);
            DataRecalculation.RecalculateMonitoringData(MonitoringOfRisks, _intervals);
            DisplayIntervals();

            #region Subscribe again
            foreach (var l in RealisationLosses)
            {
                l.PropertyChanged += RealisationLosses_PropertyChanged;
            }
            #endregion
        }
        #endregion
    }
}
