﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Xml.Linq;

namespace AVPZ.Lab5.Models
{
    public class SourceOfRisk : INotifyPropertyChanged
    {
        private string _sourceText;
        private int _count;
        private double? _percent;
        public string SourceText
        {
            get
            {
                return _sourceText;
            }
            set
            {
                SetField(ref _sourceText, value, nameof(SourceText));
            }
        }
        public int Count
        {
            get { return _count; }
            set
            {
                //_count = value;
                SetField(ref _count, value, nameof(Count));
                OnCountChanged();
            }
        }
        public double? Percent
        {
            get { return _percent; }
            set
            {
                //_percent = value;
                SetField(ref _percent, value, nameof(Percent));
            }
        }
        public bool ModifiedFromGrid { get; set; } = false;

        public event PropertyChangedEventHandler? PropertyChanged;
        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        protected bool SetField<T>(ref T field, T value, string propertyName)
        {
            if (EqualityComparer<T>.Default.Equals(field, value)) return false;
            field = value;
            OnPropertyChanged(propertyName);
            return true;
        }

        public event EventHandler? CountChanged;

        protected virtual void OnCountChanged()
        {
            if(ModifiedFromGrid)
            {
                CountChanged?.Invoke(this, EventArgs.Empty);
            }
        }
    }
}
