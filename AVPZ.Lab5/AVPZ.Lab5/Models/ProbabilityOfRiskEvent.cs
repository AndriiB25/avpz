﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AVPZ.Lab5.Models
{
    /// <summary>
    /// Визначення ймовірності настання ризикових подій
    /// </summary>
    public class ProbabilityOfRiskEvent : INotifyPropertyChanged
    {
        #region Fields

        private string _eventName;
        private int _count;

        #region Оцінки

        private double _firstExpertMark;
        private double _secondExpertMark;
        private double _thirdExpertMark;
        private double _fourthExpertMark;
        private double _fifthExpertMark;
        private double _sixthExpertMark;
        private double _seventhExpertMark;
        private double _eighthExpertMark;
        private double _ninthExpertMark;
        private double _tenthExpertMark;
        private double _sumOrAverage;
        private double _firstExpertMarkValued;
        private double _secondExpertMarkValued;
        private double _thirdExpertMarkValued;
        private double _fourthExpertMarkValued;
        private double _fifthExpertMarkValued;
        private double _sixthExpertMarkValued;
        private double _seventhExpertMarkValued;
        private double _eighthExpertMarkValued;
        private double _ninthExpertMarkValued;
        private double _tenthExpertMarkValued;

        #endregion

        private double _probability;
        private string _probabilityAsText;
        #endregion
        #region Properties
        public string EventName
        {
            get
            {
                return _eventName;
            }
            set
            {
                _eventName = value;
                OnPropertyChanged(nameof(EventName));
            }
        }
        public int Count
        {
            get
            {
                return _count;
            }
            set
            {
                _count = value;
                OnPropertyChanged(nameof(Count));
            }
        }

        #region Оцінки експертів у діапазоні [0...1]
        public double FirstExpertMark
        {
            get
            {
                return _firstExpertMark;
            }
            set
            {
                _firstExpertMark = value;
                OnPropertyChanged(nameof(FirstExpertMark));
            }
        }
        public double SecondExpertMark
        {
            get
            {
                return _secondExpertMark;
            }
            set
            {
                _secondExpertMark = value;
                OnPropertyChanged(nameof(SecondExpertMark));
            }
        }
        public double ThirdExpertMark
        {
            get
            {
                return _thirdExpertMark;
            }
            set
            {
                _thirdExpertMark = value;
                OnPropertyChanged(nameof(ThirdExpertMark));
            }
        }
        public double FourthExpertMark
        {
            get
            {
                return _fourthExpertMark;
            }
            set
            {
                _fourthExpertMark = value;
                OnPropertyChanged(nameof(FourthExpertMark));
            }
        }
        public double FifthExpertMark
        {
            get
            {
                return _fifthExpertMark;
            }
            set
            {
                _fifthExpertMark = value;
                OnPropertyChanged(nameof(FifthExpertMark));
            }
        }
        public double SixthExpertMark
        {
            get
            {
                return _sixthExpertMark;
            }
            set
            {
                _sixthExpertMark = value;
                OnPropertyChanged(nameof(SixthExpertMark));
            }
        }
        public double SeventhExpertMark
        {
            get
            {
                return _seventhExpertMark;
            }
            set
            {
                _seventhExpertMark = value;
                OnPropertyChanged(nameof(SeventhExpertMark));
            }
        }
        public double EighthExpertMark
        {
            get
            {
                return _eighthExpertMark;
            }
            set
            {
                _eighthExpertMark = value;
                OnPropertyChanged(nameof(EighthExpertMark));
            }
        }
        public double NinthExpertMark
        {
            get
            {
                return _ninthExpertMark;
            }
            set
            {
                _ninthExpertMark = value;
                OnPropertyChanged(nameof(NinthExpertMark));
            }
        }
        public double TenthExpertMark
        {
            get
            {
                return _tenthExpertMark;
            }
            set
            {
                _tenthExpertMark = value;
                OnPropertyChanged(nameof(TenthExpertMark));
            }
        }
        #endregion

        public double SumOrAverage
        {
            get
            {
                return _sumOrAverage;
            }
            set
            {
                _sumOrAverage = value;
                OnPropertyChanged(nameof(SumOrAverage));
            }
        }

        #region Оцінки експертів з урахуванням їхньої вагомості
        public double FirstExpertMarkValued
        {
            get
            {
                return _firstExpertMarkValued;
            }
            set
            {
                _firstExpertMarkValued = value;
                OnPropertyChanged(nameof(FirstExpertMarkValued));
            }
        }
        public double SecondExpertMarkValued
        {
            get
            {
                return _secondExpertMarkValued;
            }
            set
            {
                _secondExpertMarkValued = value;
                OnPropertyChanged(nameof(SecondExpertMarkValued));
            }
        }
        public double ThirdExpertMarkValued
        {
            get
            {
                return _thirdExpertMarkValued;
            }
            set
            {
                _thirdExpertMarkValued = value;
                OnPropertyChanged(nameof(ThirdExpertMarkValued));
            }
        }
        public double FourthExpertMarkValued
        {
            get
            {
                return _fourthExpertMarkValued;
            }
            set
            {
                _fourthExpertMarkValued = value;
                OnPropertyChanged(nameof(FourthExpertMarkValued));
            }
        }
        public double FifthExpertMarkValued
        {
            get
            {
                return _fifthExpertMarkValued;
            }
            set
            {
                _fifthExpertMarkValued = value;
                OnPropertyChanged(nameof(FifthExpertMarkValued));
            }
        }
        public double SixthExpertMarkValued
        {
            get
            {
                return _sixthExpertMarkValued;
            }
            set
            {
                _sixthExpertMarkValued = value;
                OnPropertyChanged(nameof(SixthExpertMarkValued));
            }
        }
        public double SeventhExpertMarkValued
        {
            get
            {
                return _seventhExpertMarkValued;
            }
            set
            {
                _seventhExpertMarkValued = value;
                OnPropertyChanged(nameof(SeventhExpertMarkValued));
            }
        }
        public double EighthExpertMarkValued
        {
            get
            {
                return _eighthExpertMarkValued;
            }
            set
            {
                _eighthExpertMarkValued = value;
                OnPropertyChanged(nameof(EighthExpertMarkValued));
            }
        }
        public double NinthExpertMarkValued
        {
            get
            {
                return _ninthExpertMarkValued;
            }
            set
            {
                _ninthExpertMarkValued = value;
                OnPropertyChanged(nameof(NinthExpertMarkValued));
            }
        }
        public double TenthExpertMarkValued
        {
            get
            {
                return _tenthExpertMarkValued;
            }
            set
            {
                _tenthExpertMarkValued = value;
                OnPropertyChanged(nameof(TenthExpertMarkValued));
            }
        }
        #endregion

        public double Probability
        {
            get { return _probability; }
            set
            {
                _probability = value;
                OnPropertyChanged(nameof(Probability));
            }
        }
        public string ProbabilityAsText
        {
            get { return _probabilityAsText; }
            set
            {
                _probabilityAsText = value;
                OnPropertyChanged(nameof(ProbabilityAsText));
            }
        }

        #endregion

        public event PropertyChangedEventHandler? PropertyChanged;

        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
