﻿using System.ComponentModel;

namespace AVPZ.Lab5.Models
{
    public class EventToReduceRisk : INotifyPropertyChanged
    {
        #region Fields
        private string _name;
        private int _avoidance;
        private int _acceptance;
        #endregion

        #region Properties
        public string Name 
        {
            get { return _name; }
            set 
            { 
                _name = value;
                OnPropertyChanged(nameof(Name));
            }
        }
        /// <summary>
        /// пом'якшення
        /// </summary>
        public int Acceptance
        {
            get { return _acceptance; }
            set
            {
                _acceptance = value;
                OnPropertyChanged(nameof(Acceptance));
            }
        }
        /// <summary>
        /// ухилення
        /// </summary>
        public int Avoidance
        {
            get { return _avoidance; }
            set
            {
                _avoidance = value;
                OnPropertyChanged(nameof(Avoidance));
            }
        }
        #endregion

        public event PropertyChangedEventHandler? PropertyChanged;

        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
