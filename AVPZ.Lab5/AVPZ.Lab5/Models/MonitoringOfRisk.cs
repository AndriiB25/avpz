﻿using System.ComponentModel;

namespace AVPZ.Lab5.Models
{
    public class MonitoringOfRisk : INotifyPropertyChanged
    {
        #region Fields
        private string _eventName;
        private int _count;
        private double _price;
        private string _priorityLevel;
        #endregion

        #region Properties
        public string EventName
        {
            get { return _eventName; }
            set 
            { 
                _eventName = value;
                OnPropertyChanged(nameof(EventName));
            }
        }
        public int Count
        {
            get { return _count; }
            set
            {
                _count = value;
                OnPropertyChanged(nameof(Count));
            }
        }
        public double Price
        {
            get { return _price; }
            set
            {
                _price = value;
                OnPropertyChanged(nameof(Price));
            }
        }
        public string PriorityLevel
        {
            get { return _priorityLevel; }
            set
            {
                _priorityLevel = value;
                OnPropertyChanged(nameof(PriorityLevel));
            }
        }
        #endregion

        public event PropertyChangedEventHandler? PropertyChanged;
        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
