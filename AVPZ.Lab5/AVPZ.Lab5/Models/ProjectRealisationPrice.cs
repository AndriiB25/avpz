﻿using System.ComponentModel;

namespace AVPZ.Lab5.Models
{
    public class ProjectRealisationPrice : INotifyPropertyChanged
    {
        private string _type;
        private double _price1;
        private double _price2;
        private double _price3;
        private double _price4;
        public string Type 
        { 
            get {  return _type; }
            set 
            {  
                _type = value;
                OnPropertyChanged(nameof(Type));
            }
        }
        public double Price1
        {
            get { return _price1; }
            set
            {
                _price1 = value;
                OnPropertyChanged(nameof(Price1));
            }
        }
        public double Price2
        {
            get { return _price2; }
            set
            {
                _price2 = value;
                OnPropertyChanged(nameof(Price2));
            }
        }
        public double Price3
        {
            get { return _price3; }
            set
            {
                _price3 = value;
                OnPropertyChanged(nameof(Price3));
            }
        }
        public double Price4
        {
            get { return _price4; }
            set
            {
                _price4 = value;
                OnPropertyChanged(nameof(Price4));
            }
        }

        public event PropertyChangedEventHandler? PropertyChanged;

        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
