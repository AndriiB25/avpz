﻿using AVPZ.Lab5.Extensions;
using AVPZ.Lab5.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AVPZ.Lab5.DataGenerators
{
    /// <summary>
    /// Клас для початкової ініціалізації таблиць
    /// </summary>
    public static class TablesDataGenerator
    {
        // 1.1
        public static List<SourceOfRisk> CreateSourcesOfRisks()
        {
            const int AmoutOfRisks = 18, TechRisksTotal = 7, PriceRisksTotal = 3, 
                PlannedRisksTotal = 3, ProjectRealisationRisksTotal = 5;
            
            var sources = new List<SourceOfRisk>();

            var techRisks = new List<SourceOfRisk>()
            {
                new SourceOfRisk() { SourceText = "1. Множина джерел появи технічних ризиків", Count = TechRisksTotal, Percent = CalculatePercent(TechRisksTotal, AmoutOfRisks) },
                new SourceOfRisk() { SourceText = " 1.1. Функціональні характеристики ПЗ", Count = 1},
                new SourceOfRisk() { SourceText = " 1.2. Характеристики якості ПЗ", Count = 1},
                new SourceOfRisk() { SourceText = " 1.3. Характеристики надійності ПЗ", Count = 1},
                new SourceOfRisk() { SourceText = " 1.4. Застосовність ПЗ", Count = 1},
                new SourceOfRisk() { SourceText = " 1.5. Часова продуктивність ПЗ", Count = 1},
                new SourceOfRisk() { SourceText = " 1.6. Супроводжуваність ПЗ", Count = 1},
                new SourceOfRisk() { SourceText = " 1.7. Повторне використання компонент ПЗ", Count = 1},
            };

            var priceRisks = new List<SourceOfRisk>()
            {
                new SourceOfRisk() { SourceText = "2. Множина джерел появи вартісних ризиків", Count = PriceRisksTotal, Percent = CalculatePercent(PriceRisksTotal, AmoutOfRisks) },
                new SourceOfRisk() { SourceText = " 2.1. Обмеження сумарного бюджету на програмний проект", Count = 1},
                new SourceOfRisk() { SourceText = " 2.2. Недоступна вартість реалізації програмного проекту", Count = 1},
                new SourceOfRisk() { SourceText = " 2.3. Низька ступінь реалізму при оцінюванні витрат на програмний проект", Count = 1},
            };

            var plannedRisks = new List<SourceOfRisk>() { 
                new SourceOfRisk() 
                {
                    SourceText = "3. Множина джерел появи планових ризиків", 
                    Count = PlannedRisksTotal, 
                    Percent = CalculatePercent(PlannedRisksTotal, AmoutOfRisks)
                },
                new SourceOfRisk() {SourceText = " 3.1. Властивості та можливості гнучкості внесення змін до планів життєвого циклу розроблення ПЗ", Count = 1},
                new SourceOfRisk() {SourceText = " 3.2. Можливості порушення встановлених термінів реалізації етапів життєвого циклу розроблення ПЗ", Count = 1},
                new SourceOfRisk() {SourceText = " 3.3. Низька ступінь реалізму при встановленні планів і етапів життєвого циклу розроблення ПЗ", Count = 1}
            };

            var projectRealisationRisks = new List<SourceOfRisk>() 
            {
                new SourceOfRisk()
                {
                    SourceText = "4.Множина джерел появи ризиків реалізації процесу управління програмним проектом",
                    Count = ProjectRealisationRisksTotal,
                    Percent = CalculatePercent(ProjectRealisationRisksTotal, AmoutOfRisks)
                },
                new SourceOfRisk() { SourceText = " 4.1. Хибна стратегія реалізації програмного проекту", Count = 1},
                new SourceOfRisk() { SourceText = " 4.2. Неефективне планування проекту розроблення ПЗ", Count = 1},
                new SourceOfRisk() { SourceText = " 4.3. Неякісне оцінювання програмного проекту", Count = 1},
                new SourceOfRisk() { SourceText = " 4.4. Прогалини в документуванні етапів реалізації програмного проекту", Count = 1},
                new SourceOfRisk() { SourceText = " 4.5. Промахи в прогнозуванні результатів реалізації програмного проекту", Count = 1},
            };
            
            sources.AddRange(techRisks);
            sources.AddRange(priceRisks);
            sources.AddRange(plannedRisks);
            sources.AddRange(projectRealisationRisks);
            sources.Add(new SourceOfRisk
            {
                SourceText = "Всього",
                Count = AmoutOfRisks,
                Percent= CalculatePercent(AmoutOfRisks, AmoutOfRisks)
            });
            
            return sources;
        }

        // 1.2
        public static List<SourceOfRisk> CreatePotentialRiskedEvents()
        {
            var riskEvents = new List<SourceOfRisk>();
            const int TechRisksTotal = 11, PriceRisksTotal = 7,
               PlannedRisksTotal = 9, ProjectRealisationRisksTotal = 14;
            const int AmoutOfRisks = TechRisksTotal + PriceRisksTotal + PlannedRisksTotal + ProjectRealisationRisksTotal;

            var techEvents = new List<SourceOfRisk>()
            {
                new SourceOfRisk() 
                { 
                    SourceText = "1. Множина настання технічних ризикових подій", 
                    Count = TechRisksTotal, 
                    Percent = CalculatePercent(TechRisksTotal, AmoutOfRisks) 
                },
                new SourceOfRisk() { SourceText = " 1.1. Затримки у постачанні обладнання, необхідного для підтримки процесу розроблення ПЗ;", Count = 1},
                new SourceOfRisk() { SourceText = " 1.2. Затримки у постачанні інструментальних засобів, необхідних для підтримки процесу розроблення ПЗ", Count = 1},
                new SourceOfRisk() { SourceText = " 1.3. Небажання команди виконавців використовувати інструментальні засоби для підтримки процесу розроблення ПЗ", Count = 1},
                new SourceOfRisk() { SourceText = " 1.4. Формування запитів на більш потужні інструментальні засоби розроблення ПЗ", Count = 1},
                new SourceOfRisk() { SourceText = " 1.5. Відмова команди виконавців від CASE-засобів розроблення ПЗ", Count = 1},
                new SourceOfRisk() { SourceText = " 1.6. Неефективність програмного коду, згенерованого CASE-засобами розроблення ПЗ", Count = 1},
                new SourceOfRisk() { SourceText = " 1.7. Неможливість інтеграції CASE-засобів з іншими інструментальними засобами для підтримки процесу розроблення ПЗ", Count = 1},
                new SourceOfRisk() { SourceText = " 1.8. Недостатня продуктивність баз(и) даних для підтримки процесу розроблення ПЗ", Count = 1},
                new SourceOfRisk() { SourceText = " 1.9. Програмні компоненти, які використовують повторно в ПЗ, мають дефекти та обмежені функціональні можливост", Count = 1},
                new SourceOfRisk() { SourceText = " 1.10. Швидкість виявлення дефектів у програмному коді є нижчою від раніше запланованих термінів", Count = 1},
                new SourceOfRisk() { SourceText = " 1.11. Поява дефектних системних компонент, які використовують для розроблення ПЗ", Count = 1},
            };

            var priceEvents = new List<SourceOfRisk>()
            {
                new SourceOfRisk() 
                { 
                    SourceText = "2. Множина настання вартісних ризикових подій", 
                    Count = PriceRisksTotal, 
                    Percent = CalculatePercent(PriceRisksTotal, AmoutOfRisks) 
                },
                new SourceOfRisk() { SourceText = " 2.1. Недо(пере)оцінювання витрат на реалізацію програмного проекту (надмірно низька вартість)", Count = 1 },
                new SourceOfRisk() { SourceText = " 2.2. Фінансові ускладнення у компанії-замовника ПЗ", Count = 1 },
                new SourceOfRisk() { SourceText = " 2.3. Фінансові ускладнення у компанії-розробника ПЗ", Count = 1 },
                new SourceOfRisk() { SourceText = " 2.4. Змен(збіль)шення бюджету програмного проекта з ініціативи компанії-замовника ПЗ під час його реалізації", Count = 1 },
                new SourceOfRisk() { SourceText = " 2.5. Висока вартість виконання повторних робіт, необхідних для зміни вимог до ПЗ", Count = 1 },
                new SourceOfRisk() { SourceText = " 2.6. Реорганізація структурних підрозділів у компанії-замовника ПЗ", Count = 1 },
                new SourceOfRisk() { SourceText = " 2.7. Реорганізація команди виконавців у компанії-розробника ПЗ", Count = 1 },
            };

            var plannedEvents = new List<SourceOfRisk>() {
                new SourceOfRisk()
                {
                    SourceText = "3. Множина настання планових ризикових подій",
                    Count = PlannedRisksTotal,
                    Percent = CalculatePercent(PlannedRisksTotal, AmoutOfRisks)
                },
                new SourceOfRisk() {SourceText = " 3.1. Зміни графіка виконання робіт з боку замовника чи розробника ПЗ", Count = 1 },
                new SourceOfRisk() {SourceText = " 3.2. Порушення графіка виконання робіт з боку компанії-розробника ПЗ", Count = 1},
                new SourceOfRisk() {SourceText = " 3.3. Потреба зміни користувацьких вимог до ПЗ з боку компанії-замовника ПЗ", Count = 1 },
                new SourceOfRisk() {SourceText = " 3.4. Потреба зміни функціональних вимог до ПЗ з боку компанії-розробника ПЗ", Count = 1 },
                new SourceOfRisk() {SourceText = " 3.5. Потреба виконання великої кількості повторних робіт, необхідних для зміни вимог до ПЗ", Count = 1 },
                new SourceOfRisk() {SourceText = " 3.6. Недо(пере)оцінювання тривалості етапів реалізації програмного проекту з боку компанії-замовника ПЗ", Count = 1 },
                new SourceOfRisk() {SourceText = " 3.7. Остаточний розмір ПЗ значно перевищує (менший від) заплановані(их) його характеристики", Count = 1 },
                new SourceOfRisk() {SourceText = " 3.8. Поява на ринку аналогічного ПЗ до виходу замовленого", Count = 1 },
                new SourceOfRisk() {SourceText = " 3.9. Поява на ринку більш конкурентоздатного ПЗ", Count = 1 }
            };

            var projectRealisationEvents = new List<SourceOfRisk>()
            {
                new SourceOfRisk()
                {
                    SourceText = "4. Множина джерел появи ризиків реалізації процесу управління програмним проектом",
                    Count = ProjectRealisationRisksTotal,
                    Percent = CalculatePercent(ProjectRealisationRisksTotal, AmoutOfRisks)
                },
                new SourceOfRisk() { SourceText = " 4.1. Низький моральний стан персоналу команди виконавців ПЗ", Count = 1 },
                new SourceOfRisk() { SourceText = " 4.2. Низька взаємодія між членами команди виконавців ПЗ", Count = 1 },
                new SourceOfRisk() { SourceText = " 4.3. Пасивність керівника (менеджера) програмного проекту", Count = 1 },
                new SourceOfRisk() { SourceText = " 4.4. Недостатня компетентність керівника (менеджера) програмного проекту", Count = 1 },
                new SourceOfRisk() { SourceText = " 4.5. Незадоволеність замовника результатами етапів реалізації програмного проекту", Count = 1},
                new SourceOfRisk() { SourceText = " 4.6. Недостатня кількість фахівців у команді виконавців ПЗ з необхідним професійним рівнем", Count = 1 },
                new SourceOfRisk() { SourceText = " 4.7. Хвороба провідного виконавця в найкритичніший момент розроблення ПЗ", Count = 1},
                new SourceOfRisk() { SourceText = " 4.8. Одночасна хвороба декількох виконавців підчас розроблення ПЗ", Count = 1 },
                new SourceOfRisk() { SourceText = " 4.9. Неможливість організації необхідного навчання персоналу команди виконавців ПЗ", Count = 1},
                new SourceOfRisk() { SourceText = " 4.10. Зміна пріоритетів у процесі управління програмним проектом", Count = 1 },
                new SourceOfRisk() { SourceText = " 4.11. Недо(пере)оцінювання необхідної кількості розробників (підрядників і субпідрядників) на етапах життєвого циклу розроблення ПЗ;", Count = 1},
                new SourceOfRisk() { SourceText = " 4.12. Недостатнє (надмірне) документування результатів на етапах реалізації програмного проекту", Count = 1 },
                new SourceOfRisk() { SourceText = " 4.13. Нереалістичне прогнозування результатів на етапах реалізації програмного проекту", Count = 1},
                new SourceOfRisk() { SourceText = " 4.14. Недостатній професійний рівень представників від компанії-замовника ПЗ", Count = 1},
            };

            riskEvents.AddRange(techEvents);
            riskEvents.AddRange(priceEvents);
            riskEvents.AddRange(plannedEvents);
            riskEvents.AddRange(projectRealisationEvents);
            riskEvents.Add(new SourceOfRisk
            {
                SourceText = "Всього",
                Count = AmoutOfRisks,
                Percent = CalculatePercent(AmoutOfRisks, AmoutOfRisks)
            });

            return riskEvents;
        }

        // 2.1 
        public static IEnumerable<ProbabilityOfRiskEvent> GenerateProbabilityOfOccurrenceOfRiskEvents(List<SourceOfRisk> sourcesOfRisks, int rowsCount, int[] weightCoeffs, double commonValue)
        {
            int Rows = rowsCount, Cols = 22;
            
            // матриця для числових значень
            var matrixOfNumbers = new double[Rows, Cols];
            var coeffsCount = weightCoeffs.Length;
            
            // заповнення частини Оцінки експертів у діапазоні [0..1]

            // копіювання коефіцієнтів вагомості (1 рядок)
            for (int i = 0; i < coeffsCount; i++)
            {
                matrixOfNumbers[0, i] = weightCoeffs[i];
            }

            // переходиме на наступний рядок
            int rowsToFill = Rows - 1, columnsToFill = coeffsCount, 
                rowStartIndex = 1, columnStartIndex = 0;
            
            const double Addend = 0.1, Multiplier = 0.7;
            
            // тепер рандомні значення
            for (int i = rowStartIndex; i <= rowsToFill; i++)
            {
                var rowMult = GenerateMultiplierForRow(commonValue);
                for (int j = columnStartIndex; j < columnsToFill; j++) 
                {
                    var columnMult = GenerateMultiplierForColumn(Addend, Multiplier);
                    var randValue = new Random().NextDouble();
                    matrixOfNumbers[i, j] = rowMult + randValue * columnMult;
                }
            }

            double sumOfExpertsCoeffs = 0;

            // стовпець сум, середніх значень по рядках
            for (int i = 0; i <= rowsToFill; i++)
            {
                if (i == 0)
                {
                    matrixOfNumbers[i, columnsToFill] = sumOfExpertsCoeffs = SumForRow(matrixOfNumbers, 0);
                }
                else
                {
                    matrixOfNumbers[i, columnsToFill] = AverageForRow(matrixOfNumbers, i);
                }
            }

            columnStartIndex = 11; columnsToFill = coeffsCount * 2;
            
            // заповнення частини Оцінки експертів з урахуванням їхньої вагомості
            for (int i = rowStartIndex; i <= rowsToFill; i++)
            {
                for (int j = columnStartIndex, k = 0; j <= columnsToFill && k < coeffsCount; j++, k++)
                {
                    matrixOfNumbers[i, j] = matrixOfNumbers[i, k] * matrixOfNumbers[0, k];
                }
            }

            // перший рядок з цієї частини де середнє по стовцях, яке ділиться на відповідний коф експерта
            for(int i = columnStartIndex, k = 0; i <= columnsToFill && k < coeffsCount; i++, k++)
            {
                matrixOfNumbers[0, i] = AverageForColumn(matrixOfNumbers, i, endRow: Rows) / matrixOfNumbers[0, k];
            }

            // останній стовпець
            var lastColumnIndex = columnsToFill + 1;

            //Debug.Write(sumOfExpertsCoeffs + "\n");
            for(int i = rowStartIndex; i <= rowsToFill; i++)
            {
                matrixOfNumbers[i, lastColumnIndex] = SumForRow(matrixOfNumbers, i, columnStartIndex, columnsToFill) / sumOfExpertsCoeffs;
            }

            // 1 рядок в цьому останньому стовпці
            matrixOfNumbers[0, columnsToFill + 1] = AverageForColumn(matrixOfNumbers, lastColumnIndex, endRow: Rows);

            // перенеесення даних
            var result = new List<ProbabilityOfRiskEvent>();
            int column = 0;
            for(int i = 0; i < matrixOfNumbers.GetLength(0); i++)
            {
                var newElement = new ProbabilityOfRiskEvent()
                {
                    EventName = sourcesOfRisks[i].SourceText,
                    Count = sourcesOfRisks[i].Count,
                    FirstExpertMark = matrixOfNumbers[i, column].Round(),
                    SecondExpertMark = matrixOfNumbers[i, ++column].Round(),
                    ThirdExpertMark = matrixOfNumbers[i, ++column].Round(),
                    FourthExpertMark = matrixOfNumbers[i, ++column].Round(),
                    FifthExpertMark = matrixOfNumbers[i, ++column].Round(),
                    SixthExpertMark = matrixOfNumbers[i, ++column].Round(),
                    SeventhExpertMark = matrixOfNumbers[i, ++column].Round(),
                    EighthExpertMark = matrixOfNumbers[i, ++column].Round(),
                    NinthExpertMark = matrixOfNumbers[i, ++column].Round(),
                    TenthExpertMark = matrixOfNumbers[i, ++column].Round(),

                    SumOrAverage = matrixOfNumbers[i, ++column].Round(),

                    FirstExpertMarkValued = matrixOfNumbers[i, ++column].Round(),
                    SecondExpertMarkValued = matrixOfNumbers[i, ++column].Round(),
                    ThirdExpertMarkValued = matrixOfNumbers[i, ++column].Round(),
                    FourthExpertMarkValued = matrixOfNumbers[i, ++column].Round(),
                    FifthExpertMarkValued = matrixOfNumbers[i, ++column].Round(),
                    SixthExpertMarkValued = matrixOfNumbers[i, ++column].Round(),
                    SeventhExpertMarkValued = matrixOfNumbers[i, ++column].Round(),
                    EighthExpertMarkValued = matrixOfNumbers[i, ++column].Round(),
                    NinthExpertMarkValued = matrixOfNumbers[i, ++column].Round(),
                    TenthExpertMarkValued = matrixOfNumbers[i, ++column].Round(),

                    Probability = matrixOfNumbers[i, ++column].Round(),
                    ProbabilityAsText = EvaluateRiskLevel(matrixOfNumbers[i, column])
                };
                column = 0;

                result.Add(newElement);
            }


            return result;
        }

        // 2.2
        public static (List<LosesFromRisk> Data, double FinalPrice) GeneratePossibleLossesFromRisk(List<SourceOfRisk> sourcesOfRisks, int rowsCount, int[] weightCoeffs, double commonValue, double firstPrice)
        {
            int Rows = rowsCount, Cols = 24; // бо + 1 спереду і +1 ззаду

            // матриця для числових значень
            var matrixOfNumbers = new double[Rows, Cols];
            var coeffsCount = weightCoeffs.Length;

            // заповнення частини Оцінки експертів у діапазоні [0..1]

            // копіювання коефіцієнтів вагомості (1 рядок)
            for (int i = 1, k = 0; k < coeffsCount; i++, k ++)
            {
                matrixOfNumbers[0, i] = weightCoeffs[k];
            }

            // переходиме на наступний рядок
            int rowsToFill = Rows - 1, columnsToFill = coeffsCount + 1,
                rowStartIndex = 1, columnStartIndex = 1;

            const double Addend = 0.2, Multiplier = 0.5;

            var randomCol = new List<double>(); // для 1 колонки
            
            // тепер рандомні значення
            for (int i = rowStartIndex; i <= rowsToFill; i++)
            {
                var rowMult = GenerateMultiplierForRow(commonValue);
                randomCol.Add(rowMult);

                for (int j = columnStartIndex; j < columnsToFill; j++)
                {
                    var columnMult = GenerateMultiplierForColumn(Addend, Multiplier);
                    var randValue = new Random().NextDouble();
                    matrixOfNumbers[i, j] = rowMult + randValue * columnMult;
                }
            }

            // 1 колонка
            var randomSum = randomCol.Sum();
            
            matrixOfNumbers[0, 0] = firstPrice;

            for(int i = rowStartIndex, k = 0; i <= rowsToFill; i++, k++)
            {
                matrixOfNumbers[i, 0] = firstPrice * randomCol[k] / randomSum;
            }

            // стовпець сум, середніх значень по рядках
            double sumOfExpertsCoeffs = 0;

            for (int i = 0; i <= rowsToFill; i++)
            {
                if (i == 0)
                {
                    matrixOfNumbers[i, columnsToFill] = sumOfExpertsCoeffs = SumForRow(matrixOfNumbers, i, columnStartIndex, columnsToFill);
                }
                else
                {
                    matrixOfNumbers[i, columnsToFill] = AverageForRow(matrixOfNumbers, i);
                }
            }

            columnStartIndex = 12; columnsToFill = coeffsCount * 2 + 1;

            // заповнення частини Оцінки експертів з урахуванням їхньої вагомості
            for (int i = rowStartIndex; i <= rowsToFill; i++)
            {
                for (int j = columnStartIndex, k = 1; j <= columnsToFill && k <= coeffsCount; j++, k++)
                {
                    matrixOfNumbers[i, j] = matrixOfNumbers[i, k] * matrixOfNumbers[0, k];
                }
            }

            // перший рядок з цієї частини де середнє по стовцях, яке ділиться на відповідний коф експерта
            for (int i = columnStartIndex, k = 0; i <= columnsToFill && k < coeffsCount; i++, k++)
            {
                matrixOfNumbers[0, i] = AverageForColumn(matrixOfNumbers, i, endRow: Rows) / matrixOfNumbers[0, k];
            }

            // стовпець додаткова вартість
            var preLastColumnIndex = columnsToFill + 1;
            var additionalPrice = new List<double>();
            for (int i = rowStartIndex; i <= rowsToFill; i++)
            {
                var value = SumForRow(matrixOfNumbers, i, columnStartIndex, columnsToFill) / sumOfExpertsCoeffs * matrixOfNumbers[i, 0];
                additionalPrice.Add(value);
                matrixOfNumbers[i, preLastColumnIndex] = value;
            }

            // var minPrice = additionalPrice.Min(); var maxPrice = additionalPrice.Max();
            // загальна додаткова вартість (1 рядок цього стовпця)
            matrixOfNumbers[0, preLastColumnIndex] = SumForColumn(matrixOfNumbers, preLastColumnIndex, rowStartIndex, rowsToFill);


            // стовпець кінцева вартість
            var lastColumnIndex = columnsToFill + 2;

            for (int i = 0; i <= rowsToFill; i++)
            {
                matrixOfNumbers[i, lastColumnIndex] = matrixOfNumbers[i, 0] + matrixOfNumbers[i, preLastColumnIndex];
            }

            var result = new List<LosesFromRisk>();
            int column = 0;
            for (int i = 0; i < matrixOfNumbers.GetLength(0); i++)
            {
                var newElement = new LosesFromRisk()
                {
                    EventName = sourcesOfRisks[i].SourceText,
                    Count = sourcesOfRisks[i].Count,
                    Price = matrixOfNumbers[i, column].Round(),
                    FirstExpertMark = matrixOfNumbers[i, ++column].Round(),
                    SecondExpertMark = matrixOfNumbers[i, ++column].Round(),
                    ThirdExpertMark = matrixOfNumbers[i, ++column].Round(),
                    FourthExpertMark = matrixOfNumbers[i, ++column].Round(),
                    FifthExpertMark = matrixOfNumbers[i, ++column].Round(),
                    SixthExpertMark = matrixOfNumbers[i, ++column].Round(),
                    SeventhExpertMark = matrixOfNumbers[i, ++column].Round(),
                    EighthExpertMark = matrixOfNumbers[i, ++column].Round(),
                    NinthExpertMark = matrixOfNumbers[i, ++column].Round(),
                    TenthExpertMark = matrixOfNumbers[i, ++column].Round(),

                    SumOrAverage = matrixOfNumbers[i, ++column].Round(),

                    FirstExpertMarkValued = matrixOfNumbers[i, ++column].Round(),
                    SecondExpertMarkValued = matrixOfNumbers[i, ++column].Round(),
                    ThirdExpertMarkValued = matrixOfNumbers[i, ++column].Round(),
                    FourthExpertMarkValued = matrixOfNumbers[i, ++column].Round(),
                    FifthExpertMarkValued = matrixOfNumbers[i, ++column].Round(),
                    SixthExpertMarkValued = matrixOfNumbers[i, ++column].Round(),
                    SeventhExpertMarkValued = matrixOfNumbers[i, ++column].Round(),
                    EighthExpertMarkValued = matrixOfNumbers[i, ++column].Round(),
                    NinthExpertMarkValued = matrixOfNumbers[i, ++column].Round(),
                    TenthExpertMarkValued = matrixOfNumbers[i, ++column].Round(),

                    AdditionalPrice = matrixOfNumbers[i, ++column].Round(),
                    FinalPrice = matrixOfNumbers[i, ++column].Round()
                };

                column = 0;

                result.Add(newElement);
            }

            return (result, matrixOfNumbers[0, lastColumnIndex]);
        }

        public static List<ProjectRealisationPrice> GenerateProjectRealisationPricesData(List<double> startPrices, List<double> finalPrices)
        {
            return new List<ProjectRealisationPrice> {
                new ProjectRealisationPrice
                {
                    Type = "Початкова",
                    Price1 = startPrices[0].Round(),
                    Price2 = startPrices[1].Round(),
                    Price3 = startPrices[2].Round(),
                    Price4 = startPrices[3].Round()
                },
                new ProjectRealisationPrice
                {
                    Type = "Кінцева",
                    Price1 = finalPrices[0].Round(),
                    Price2 = finalPrices[1].Round(),
                    Price3 = finalPrices[2].Round(),
                    Price4 = finalPrices[3].Round()
                }
            };
        }

        // 3.1
        public static List<EventToReduceRisk> CreateEventsToReduceRisks()
        {
            return new List<EventToReduceRisk>
            {
                new EventToReduceRisk { Name = "Попереднє навчання членів проектного колективу",  Acceptance = 1, Avoidance = 0 },
                new EventToReduceRisk { Name = "Узгодження детального переліку вимог до ПЗ із замовником",  Acceptance = 1, Avoidance = 0 },
                new EventToReduceRisk { Name = "Внесення узгодженого переліку вимог до ПЗ замовника в договір",  Acceptance = 1, Avoidance = 0 },
                new EventToReduceRisk { Name = "Точне слідування вимогам замовника з узгодженого переліку вимог до ПЗ",  Acceptance = 1, Avoidance = 0 },
                new EventToReduceRisk { Name = "Попередні дослідження ринку",  Acceptance = 1, Avoidance = 0 },
                new EventToReduceRisk { Name = "Експертна оцінка програмного проекту досвідченим стороннім консультантом",  Acceptance = 0, Avoidance = 1 },
                new EventToReduceRisk { Name = "Консультації досвідченого стороннього консультанта",  Acceptance = 1, Avoidance = 0 },
                new EventToReduceRisk { Name = "Тренінг з вивчення необхідних інструментів розроблення ПЗ",  Acceptance = 0, Avoidance = 1 },
                new EventToReduceRisk { Name = "Укладання договору страхування",  Acceptance = 1, Avoidance = 0 },
                new EventToReduceRisk { Name = "Використання \"шаблонних\" рішень з вдалих попередніх проектів при управлінні програмним проектом",  Acceptance = 1, Avoidance = 0 },
                new EventToReduceRisk { Name = "Підготовка документів, які показують важливість даного проекту для досягнення фінансових цілей компанії-розробника",  Acceptance = 1, Avoidance = 0 },
                new EventToReduceRisk { Name = "Реорганізація роботи проектного колективу так, щоб обов'язки та робота членів колективу перекривали один одного",  Acceptance = 0, Avoidance = 1 },
                new EventToReduceRisk { Name = "Придбання (замовлення) частини компонент розроблюваного ПЗ",  Acceptance = 1, Avoidance = 0 },
                new EventToReduceRisk { Name = "Заміна потенційно дефектних компонент розроблюваного ПЗ придбаними компонентами, які гарантують якість виконання роботи",  Acceptance = 1, Avoidance = 0 },
                new EventToReduceRisk { Name = "Придбання більш продуктивної бази даних",  Acceptance = 1, Avoidance = 0 },
                new EventToReduceRisk { Name = "Використання генератора програмного коду",  Acceptance = 1, Avoidance = 0 },
                new EventToReduceRisk { Name = "Реорганізація роботи проектного колективу залежно від рівня труднощів виконання завдань та професійних рівнів розробників",  Acceptance = 0, Avoidance = 1 },
                new EventToReduceRisk { Name = "Повторне використання придатних компонент ПЗ, які були розроблені для інших програмних проектів",  Acceptance = 1, Avoidance = 0 },
                new EventToReduceRisk { Name = "Аналіз доцільності розроблення даного ПЗ",  Acceptance = 1, Avoidance = 0 },
            };
        }

        // 4.1
        public static (List<MonitoringOfRisk> MonitoringData, List<(double Start, double End)> Intervals) 
            GenerateMonitoringData(List<(string EventName, int Count, double Price)> techRisks, List<(string EventName, int Count, double Price)> priceRisks, 
                                            List<(string EventName, int Count, double Price)> plannedRisks, List<(string EventName, int Count, double Price)> reasilationRisks)
        {
            static (double Max, double Min) ProcessData(List<(string EventName, int Count, double Price)> risks, List<MonitoringOfRisk> result)
            {
                var tempList = new List<MonitoringOfRisk>();
                foreach (var v in risks)
                {
                    tempList.Add(new MonitoringOfRisk()
                    {
                        EventName = v.EventName,
                        Count = v.Count,
                        Price = v.Price
                    });
                }

                result.AddRange(tempList);
                var maxPrice = tempList.Skip(1).Select(x => x.Price).Max();
                var minPrice = tempList.Skip(1).Select(x => x.Price).Min();

                return (maxPrice, minPrice);
            }

            static string SetUpPriorityLevel(double price, (double Start, double End) firstInterval, (double Start, double End) secondInterval, (double Start, double End) thirdInterval)
            {
                if (price < firstInterval.Start)
                {
                    return "ДУЖЕ НИЗЬКИЙ";
                }
                else if (price >= firstInterval.Start && price < firstInterval.End)
                {
                    return "НИЗЬКИЙ";
                }

                else if (price >= secondInterval.Start && price < secondInterval.End)
                {
                    return "СЕРЕДНІЙ";
                }
                else if (price >= thirdInterval.Start && price <= thirdInterval.End)
                {
                    return "ВИСОКИЙ";
                }
                else
                {
                    return "";
                }
            }

            var result = new List<MonitoringOfRisk>();
            var maxMins = new List<(double Max, double Min)>
            {
                // перенесення даних
                ProcessData(techRisks, result),
                ProcessData(priceRisks, result),
                ProcessData(plannedRisks, result),
                ProcessData(reasilationRisks, result),
            };
            
            var max = maxMins.Select(x => x.Max).Max();
            var min = maxMins.Select(x => x.Min).Min();

            var intervalLength = Math.Abs(max - min) / 3;

            // створення проміжків
            (double Start, double End) firstInterval = (min.Round(), (min + intervalLength).Round());
            (double Start, double End) secondInterval = (firstInterval.End, (firstInterval.End + intervalLength).Round());
            (double Start, double End) thirdInterval = (secondInterval.End, max.Round());

            var intervals = new List<(double Start, double End)>() { firstInterval, secondInterval, thirdInterval };

            foreach(var m in result)
            {
                m.PriorityLevel = SetUpPriorityLevel(m.Price, firstInterval, secondInterval, thirdInterval);
            }

            return (result, intervals);
        }

        // Коефіцієнти вагомості кожного з експертів за множинами ризиків
        public static (int[] Tech, int[] Valued, int[] Planned, int[] Realisation) GenerateWeightingCoefficientsOfTheExperts(int reduceValue)
        {
            // обмеження згенерованої оцінки до 10
            int ReduceValue(int value)
            {
                const int maxValue = 10;
                return value > maxValue ? maxValue : value;
            }

            // рандомні множники
            var techRisksMult = GenerateMultiplierForMarks(); // тех
            var valuedRisksMult = GenerateMultiplierForMarks(); // вартісні
            var plannedRisksMult = GenerateMultiplierForMarks(); // планові
            var realisationRisksMult = GenerateMultiplierForMarks(); // реалізації
            const int ArraySize = 10;
            int[] techMarks = new int[ArraySize], valuedMarks = new int[ArraySize],
                plannedMarks = new int[ArraySize], realisationMarks = new int[ArraySize];

            for (int i = 0; i < ArraySize; i++)
            {
                techMarks[i] = ReduceValue(GenerateCoeficientForExpert(techRisksMult, reduceValue));
                valuedMarks[i] = ReduceValue(GenerateCoeficientForExpert(valuedRisksMult, reduceValue));
                plannedMarks[i] = ReduceValue(GenerateCoeficientForExpert(plannedRisksMult, reduceValue));
                realisationMarks[i] = ReduceValue(GenerateCoeficientForExpert(realisationRisksMult, reduceValue));
            }

            return (techMarks, valuedMarks, plannedMarks, realisationMarks);
        }

        // Розподіл лише початкової вартості реалізації проекту за множинами ризиків
        public static double[] GenerateProjectRealisationPrices()
        {
            const int ArrayLength = 4;
            double[] startPrices = new double[ArrayLength];

            var randomArray = RandomArray();
            var commonMult = GenerateCommonMultiplierForStartPrice();
            for (int i = 0; i < ArrayLength - 1; i++)
            {
                var value = Math.Ceiling(randomArray[i] * commonMult);
                startPrices[i] = value;
            }

            startPrices[^1] = commonMult - startPrices.Sum();

            return startPrices;
        }

        #region Helper methods
        // ----------------------------------------------------------------------------
        // ДОПОМІЖНІ МЕТОДИ
        private static double AverageForRow(double[,] matrix, int row, int startColumn = 0, int endColumn = 10)
        {
            double sum = 0.0;
            int col = startColumn;
            for (; col < endColumn; col++)
            {
                sum += matrix[row, col];
            }

            return sum / col;
        }

        private static double SumForRow(double[,] matrix, int row, int startColumn = 0, int endColumn = 10)
        {
            double sum = 0;

            for (int col = startColumn; col < endColumn; col++)
            {
                sum += matrix[row, col];
            }

            return sum;
        }

        private static double AverageForColumn(double[,] matrix, int column, int startRow = 1, int endRow = 11)
        {
            double sum = 0;
            int row = startRow;
            for (; row < endRow; row++)
            {
                sum += matrix[row, column];
            }

            return sum / row;
        }

        private static double SumForColumn(double[,] matrix, int column, int startRow = 1, int endRow = 10)
        {
            double sum = 0;

            for (int row = startRow; row < endRow; row++)
            {
                sum += matrix[row, column];
            }

            return sum;
        }

        private static double CalculatePercent(int num1, int num2)
        {
            return Math.Round((double)num1 / num2 * 100, 2);
        }

        // дичка рандому, рандомний масив для розрахунку вартості
        private static double[] RandomArray()
        {
            const int ArraySize = 4, Divider = 3;
            double[] temp = new double[ArraySize], result = new double[ArraySize];
            int[] ints = { 11, 7, 9, 14, 0 };
            
            for(int i = 0; i < ArraySize; i++)
            {
                var rnd = new Random();
                temp[i] = ints[i] + rnd.NextDouble() * ints[i + 1] / Divider;
            }

            var sum = temp.Sum();
            for(int i = 0; i < ArraySize; i++)
            {
                result[i] = temp[i] / sum;
            }

            return result;
        }

        // рандомні множники для вартості
        private static double GenerateCommonMultiplierForStartPrice()
        {
            const int Addend = 300, Multiplier = 1000, Divider = 2;
            var random1 = new Random().NextDouble();
            var val1 = Addend + random1 * Multiplier;
            var random2 = new Random().NextDouble();
            return Math.Ceiling(val1 + random2 * val1 / Divider);
        }

        // рандомні множники для оцінок експертів
        private static double GenerateMultiplierForRow(double commonValue)
        {
            var randValue = new Random().NextDouble();
            return commonValue + randValue * commonValue;
        }

        private static double GenerateMultiplierForColumn(double addend, double multiplier)
        {
            var randValue = new Random().NextDouble();
            return addend + randValue * multiplier;
        }

        // рандомний множник для оцінки
        private static double GenerateMultiplierForMarks()
        {
            const int Addend = 5, Multiplier = 3;
            Random random = new Random();
            double randomValue = random.NextDouble(); // Generates a random number between 0 and 1
            double result = Addend + randomValue * Multiplier;
            return result;
        }

        // генерація коефіцієнта вагомості
        private static int GenerateCoeficientForExpert(double multiplier, int reduced)
        {
            Random random = new Random();
            int result = (int)(multiplier + random.NextDouble() * (reduced - multiplier));
            return result;
        }

        private static string EvaluateRiskLevel(double value)
        {
            return value switch
            {
                < 0.1 => "ДУЖЕ НИЗЬКОЮ",
                >= 0.1 and < 0.25 => "НИЗЬКОЮ",
                >= 0.25 and < 0.5 => "СЕРЕДНЬОЮ",
                >= 0.5 and < 0.75 => "ВИСОКОЮ",
                _ => "ДУЖЕ ВИСОКОЮ",
            };
        }
#endregion
    }
}
