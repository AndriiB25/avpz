﻿using AVPZ.Lab5.Extensions;
using AVPZ.Lab5.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace AVPZ.Lab5.DataGenerators
{
    public static class DataRecalculation
    {
        public static void RecalculateSourcesOfRisks(Collection<SourceOfRisk> sourcesOfRisks)
        {
            const int TechRisksTotal = 7, PriceRisksTotal = 3,
               PlannedRisksTotal = 3, RealisationRisksTotal = 5,
               TechPos = 0, PricePos = 8, PlannedPos = 12, RealisationPos = 16;

            var techTotal = sourcesOfRisks.Skip(TechPos + 1).Take(TechRisksTotal).Sum(x => x.Count);
            var priceTotal = sourcesOfRisks.Skip(PricePos + 1).Take(PriceRisksTotal).Sum(x => x.Count);
            var plannedTotal = sourcesOfRisks.Skip(PlannedPos + 1).Take(PlannedRisksTotal).Sum(x => x.Count);
            var realisationTotal = sourcesOfRisks.Skip(RealisationPos + 1).Take(RealisationRisksTotal).Sum(x => x.Count);

            sourcesOfRisks[TechPos].Count = techTotal;
            sourcesOfRisks[PricePos].Count = priceTotal;
            sourcesOfRisks[PlannedPos].Count = plannedTotal;
            sourcesOfRisks[RealisationPos].Count = realisationTotal;

            var totalCount = techTotal + priceTotal + plannedTotal + realisationTotal;
            sourcesOfRisks[^1].Count = totalCount;

            sourcesOfRisks[TechPos].Percent = CalculatePercent(techTotal, totalCount);
            sourcesOfRisks[PricePos].Percent = CalculatePercent(priceTotal, totalCount);
            sourcesOfRisks[PlannedPos].Percent = CalculatePercent(plannedTotal, totalCount);
            sourcesOfRisks[RealisationPos].Percent = CalculatePercent(realisationTotal, totalCount);
            sourcesOfRisks[^1].Percent = CalculatePercent(totalCount, totalCount);
        }

        public static void RecalculatePotentialRisks(Collection<SourceOfRisk> potentialRiskEvents, 
            Collection<ProbabilityOfRiskEvent> techProbs, Collection<ProbabilityOfRiskEvent> priceProbs,
            Collection<ProbabilityOfRiskEvent> plannedProbs, Collection<ProbabilityOfRiskEvent> realisationProbs,
            Collection<LosesFromRisk> techLosses, Collection<LosesFromRisk> priceLosses,
            Collection<LosesFromRisk> plannedLosses, Collection<LosesFromRisk> realisationLosses, Collection<MonitoringOfRisk> monitoringOfRisks)
        {
            const int TechRisksTotal = 11, PriceRisksTotal = 7,
              PlannedRisksTotal = 9, RealisationRisksTotal = 14,
              TechPos = 0, PricePos = 12, PlannedPos = 20, RealisationPos = 30;

            var techTotal = potentialRiskEvents.Skip(TechPos + 1).Take(TechRisksTotal).Sum(x => x.Count);
            var priceTotal = potentialRiskEvents.Skip(PricePos + 1).Take(PriceRisksTotal).Sum(x => x.Count);
            var plannedTotal = potentialRiskEvents.Skip(PlannedPos + 1).Take(PlannedRisksTotal).Sum(x => x.Count);
            var realisationTotal = potentialRiskEvents.Skip(RealisationPos + 1).Take(RealisationRisksTotal).Sum(x => x.Count);

            potentialRiskEvents[TechPos].Count = techTotal;
            potentialRiskEvents[PricePos].Count = priceTotal;
            potentialRiskEvents[PlannedPos].Count = plannedTotal;
            potentialRiskEvents[RealisationPos].Count = realisationTotal;

            var totalCount = techTotal + priceTotal + plannedTotal + realisationTotal;
            potentialRiskEvents[^1].Count = totalCount;

            potentialRiskEvents[TechPos].Percent = CalculatePercent(techTotal, totalCount);
            potentialRiskEvents[PricePos].Percent = CalculatePercent(priceTotal, totalCount);
            potentialRiskEvents[PlannedPos].Percent = CalculatePercent(plannedTotal, totalCount);
            potentialRiskEvents[RealisationPos].Percent = CalculatePercent(realisationTotal, totalCount);
            potentialRiskEvents[^1].Percent = CalculatePercent(totalCount, totalCount);

            // копіювання значень в таблиці 2.1 та 2.2 та 4.1
            CopyCount(from: potentialRiskEvents, to1: techProbs, to2: techLosses, start: 0, end: TechRisksTotal + 1);
            CopyCount(from: potentialRiskEvents, to1: priceProbs, to2:priceLosses, start: PricePos, end: PriceRisksTotal + 1);
            CopyCount(from: potentialRiskEvents, to1: plannedProbs, to2: plannedLosses, start: PlannedPos, end: PlannedRisksTotal + 1);
            CopyCount(from: potentialRiskEvents, to1: realisationProbs, to2: realisationLosses, start: RealisationRisksTotal, end: RealisationRisksTotal + 1);
            CopyCount(from: potentialRiskEvents, to: monitoringOfRisks);
        }

        public static void RecalculateRiskProbability(Collection<ProbabilityOfRiskEvent> probabilityOfRiskEvents)
        {
            // проводим обчислення лише для подій де кількість > 0
            double CheckResult(int rowIndex, double value)
            {
                return probabilityOfRiskEvents[rowIndex].Count > 0 ? value.Round() : 0;
            }

            int rowsCount = probabilityOfRiskEvents.Count;

            // перераховуємо стовпець сум та середніх значень
            for (int i = 0; i < rowsCount; i++)
            {
                if (i == 0)
                {
                    probabilityOfRiskEvents[i].SumOrAverage = GetMarksSum(probabilityOfRiskEvents, i).Round();
                }
                else
                {
                    probabilityOfRiskEvents[i].SumOrAverage = GetMarksAverage(probabilityOfRiskEvents, i).Round();
                }
            }

            // переобчислення частини Оцінки експертів з урахуванням їхньої вагомості
            for (int i = 1; i < rowsCount; i++)
            {
                probabilityOfRiskEvents[i].FirstExpertMarkValued = CheckResult(i, probabilityOfRiskEvents[i].FirstExpertMark * probabilityOfRiskEvents[0].FirstExpertMark);
                probabilityOfRiskEvents[i].SecondExpertMarkValued = CheckResult(i, probabilityOfRiskEvents[i].SecondExpertMark * probabilityOfRiskEvents[0].SecondExpertMark);
                probabilityOfRiskEvents[i].ThirdExpertMarkValued = CheckResult(i, probabilityOfRiskEvents[i].ThirdExpertMark * probabilityOfRiskEvents[0].ThirdExpertMark);
                probabilityOfRiskEvents[i].FourthExpertMarkValued = CheckResult(i, probabilityOfRiskEvents[i].FourthExpertMark * probabilityOfRiskEvents[0].FourthExpertMark);
                probabilityOfRiskEvents[i].FifthExpertMarkValued = CheckResult(i, probabilityOfRiskEvents[i].FifthExpertMark * probabilityOfRiskEvents[0].FifthExpertMark);
                probabilityOfRiskEvents[i].SixthExpertMarkValued = CheckResult(i, probabilityOfRiskEvents[i].SixthExpertMark * probabilityOfRiskEvents[0].SixthExpertMark);
                probabilityOfRiskEvents[i].SeventhExpertMarkValued = CheckResult(i, probabilityOfRiskEvents[i].SeventhExpertMark * probabilityOfRiskEvents[0].SeventhExpertMark);
                probabilityOfRiskEvents[i].EighthExpertMarkValued = CheckResult(i, probabilityOfRiskEvents[i].EighthExpertMark * probabilityOfRiskEvents[0].EighthExpertMark);
                probabilityOfRiskEvents[i].NinthExpertMarkValued = CheckResult(i, probabilityOfRiskEvents[i].NinthExpertMark * probabilityOfRiskEvents[0].NinthExpertMark);
                probabilityOfRiskEvents[i].TenthExpertMarkValued = CheckResult(i, probabilityOfRiskEvents[i].TenthExpertMark * probabilityOfRiskEvents[0].TenthExpertMark);
            }

            #region Calculate average on column
            // перераховування 1 рядка в цій частині - середнього значення по стовпцю поділеного на відповідну оцінку експерта
            var firstValued = probabilityOfRiskEvents.Skip(1).Select(p => p.FirstExpertMarkValued).Where(x => x > 0);
            probabilityOfRiskEvents[0].FirstExpertMarkValued = (firstValued.Sum() / firstValued.Count() / probabilityOfRiskEvents[0].FirstExpertMark).Round();

            var secondValued = probabilityOfRiskEvents.Skip(1).Select(p => p.SecondExpertMarkValued).Where(x => x > 0);
            probabilityOfRiskEvents[0].SecondExpertMarkValued = (secondValued.Sum() / secondValued.Count() / probabilityOfRiskEvents[0].SecondExpertMark).Round();

            var thirdValued = probabilityOfRiskEvents.Skip(1).Select(p => p.ThirdExpertMarkValued).Where(x => x > 0);
            probabilityOfRiskEvents[0].ThirdExpertMarkValued = (thirdValued.Sum() / thirdValued.Count() / probabilityOfRiskEvents[0].ThirdExpertMark).Round();

            var fourthValued = probabilityOfRiskEvents.Skip(1).Select(p => p.FourthExpertMarkValued).Where(x => x > 0);
            probabilityOfRiskEvents[0].FourthExpertMarkValued = (fourthValued.Sum() / fourthValued.Count() / probabilityOfRiskEvents[0].FourthExpertMark).Round();

            var fifthValued = probabilityOfRiskEvents.Skip(1).Select(p => p.FifthExpertMarkValued).Where(x => x > 0);
            probabilityOfRiskEvents[0].FifthExpertMarkValued = (fifthValued.Sum() / fifthValued.Count() / probabilityOfRiskEvents[0].FifthExpertMark).Round();

            var sixthValued = probabilityOfRiskEvents.Skip(1).Select(p => p.SixthExpertMarkValued).Where(x => x > 0);
            probabilityOfRiskEvents[0].SixthExpertMarkValued = (sixthValued.Sum() / sixthValued.Count() / probabilityOfRiskEvents[0].SixthExpertMark).Round();

            var seventhValued = probabilityOfRiskEvents.Skip(1).Select(p => p.SeventhExpertMarkValued).Where(x => x > 0);
            probabilityOfRiskEvents[0].SeventhExpertMarkValued = (seventhValued.Sum() / seventhValued.Count() / probabilityOfRiskEvents[0].SeventhExpertMark).Round();

            var eighthValued = probabilityOfRiskEvents.Skip(1).Select(p => p.EighthExpertMarkValued).Where(x => x > 0);
            probabilityOfRiskEvents[0].EighthExpertMarkValued = (eighthValued.Sum() / eighthValued.Count() / probabilityOfRiskEvents[0].EighthExpertMark).Round();

            var ninthValued = probabilityOfRiskEvents.Skip(1).Select(p => p.NinthExpertMarkValued).Where(x => x > 0);
            probabilityOfRiskEvents[0].NinthExpertMarkValued = (ninthValued.Sum() / ninthValued.Count() / probabilityOfRiskEvents[0].NinthExpertMark).Round();

            var tenthValued = probabilityOfRiskEvents.Skip(1).Select(p => p.TenthExpertMarkValued).Where(x => x > 0);
            probabilityOfRiskEvents[0].TenthExpertMarkValued = (tenthValued.Sum() / tenthValued.Count() / probabilityOfRiskEvents[0].TenthExpertMark).Round();
            #endregion

            // переобчислення останньої колонки - сума по рядку поділена на суму оцінок експертів
            var expertMarksSum = probabilityOfRiskEvents[0].SumOrAverage;
            for(int i = 1; i < rowsCount; i++)
            {
                var value = (GetValuedMarksSum(probabilityOfRiskEvents ,i) / expertMarksSum).Round();
                probabilityOfRiskEvents[i].Probability = value;
                probabilityOfRiskEvents[i].ProbabilityAsText = EvaluateRiskLevel(value);
            }

            // 1 рядок в останній колонці - середнє значення
            var averageForColumn = GetProbabilitiesAverage(probabilityOfRiskEvents).Round();
            probabilityOfRiskEvents[0].Probability = averageForColumn;
            probabilityOfRiskEvents[0].ProbabilityAsText = EvaluateRiskLevel(averageForColumn);
        }

        public static void RecalculateRiskLosses(Collection<LosesFromRisk> losesFromRisks, Collection<ProjectRealisationPrice> projectRealisationPrices, int priceIndex,
            Collection<MonitoringOfRisk> monitoringOfRisks, int copyStart, int copyEnd)
        {
            // проводим обчислення лише для подій де кількість > 0
            double CheckResult(int rowIndex, double value)
            {
                return losesFromRisks[rowIndex].Count > 0 ? value.Round() : 0;
            }

            int rowsCount = losesFromRisks.Count;

            // перераховуємо стовпець сум та середніх значень
            for (int i = 0; i < rowsCount; i++)
            {
                if (i == 0)
                {
                    losesFromRisks[i].SumOrAverage = GetMarksSum(losesFromRisks, i).Round();
                }
                else
                {
                    losesFromRisks[i].SumOrAverage = (GetMarksAverage(losesFromRisks, i) / losesFromRisks[i].Price).Round();
                }
            }

            // переобчислення частини Оцінки експертів з урахуванням їхньої вагомості
            for (int i = 1; i < rowsCount; i++)
            {
                losesFromRisks[i].FirstExpertMarkValued = CheckResult(i, losesFromRisks[i].FirstExpertMark * losesFromRisks[0].FirstExpertMark);
                losesFromRisks[i].SecondExpertMarkValued = CheckResult(i, losesFromRisks[i].SecondExpertMark * losesFromRisks[0].SecondExpertMark);
                losesFromRisks[i].ThirdExpertMarkValued = CheckResult(i, losesFromRisks[i].ThirdExpertMark * losesFromRisks[0].ThirdExpertMark);
                losesFromRisks[i].FourthExpertMarkValued = CheckResult(i, losesFromRisks[i].FourthExpertMark * losesFromRisks[0].FourthExpertMark);
                losesFromRisks[i].FifthExpertMarkValued = CheckResult(i, losesFromRisks[i].FifthExpertMark * losesFromRisks[0].FifthExpertMark);
                losesFromRisks[i].SixthExpertMarkValued = CheckResult(i, losesFromRisks[i].SixthExpertMark * losesFromRisks[0].SixthExpertMark);
                losesFromRisks[i].SeventhExpertMarkValued = CheckResult(i, losesFromRisks[i].SeventhExpertMark * losesFromRisks[0].SeventhExpertMark);
                losesFromRisks[i].EighthExpertMarkValued = CheckResult(i, losesFromRisks[i].EighthExpertMark * losesFromRisks[0].EighthExpertMark);
                losesFromRisks[i].NinthExpertMarkValued = CheckResult(i, losesFromRisks[i].NinthExpertMark * losesFromRisks[0].NinthExpertMark);
                losesFromRisks[i].TenthExpertMarkValued = CheckResult(i, losesFromRisks[i].TenthExpertMark * losesFromRisks[0].TenthExpertMark);
            }

            #region Calculate average on column
            // перераховування 1 рядка в цій частині - середнього значення по стовпцю поділеного на відповідну оцінку експерта
            var firstValued = losesFromRisks.Skip(1).Select(p => p.FirstExpertMarkValued).Where(x => x > 0);
            losesFromRisks[0].FirstExpertMarkValued = (firstValued.Sum() / firstValued.Count() / losesFromRisks[0].FirstExpertMark).Round();

            var secondValued = losesFromRisks.Skip(1).Select(p => p.SecondExpertMarkValued).Where(x => x > 0);
            losesFromRisks[0].SecondExpertMarkValued = (secondValued.Sum() / secondValued.Count() / losesFromRisks[0].SecondExpertMark).Round();

            var thirdValued = losesFromRisks.Skip(1).Select(p => p.ThirdExpertMarkValued).Where(x => x > 0);
            losesFromRisks[0].ThirdExpertMarkValued = (thirdValued.Sum() / thirdValued.Count() / losesFromRisks[0].ThirdExpertMark).Round();

            var fourthValued = losesFromRisks.Skip(1).Select(p => p.FourthExpertMarkValued).Where(x => x > 0);
            losesFromRisks[0].FourthExpertMarkValued = (fourthValued.Sum() / fourthValued.Count() / losesFromRisks[0].FourthExpertMark).Round();

            var fifthValued = losesFromRisks.Skip(1).Select(p => p.FifthExpertMarkValued).Where(x => x > 0);
            losesFromRisks[0].FifthExpertMarkValued = (fifthValued.Sum() / fifthValued.Count() / losesFromRisks[0].FifthExpertMark).Round();

            var sixthValued = losesFromRisks.Skip(1).Select(p => p.SixthExpertMarkValued).Where(x => x > 0);
            losesFromRisks[0].SixthExpertMarkValued = (sixthValued.Sum() / sixthValued.Count() / losesFromRisks[0].SixthExpertMark).Round();

            var seventhValued = losesFromRisks.Skip(1).Select(p => p.SeventhExpertMarkValued).Where(x => x > 0);
            losesFromRisks[0].SeventhExpertMarkValued = (seventhValued.Sum() / seventhValued.Count() / losesFromRisks[0].SeventhExpertMark).Round();

            var eighthValued = losesFromRisks.Skip(1).Select(p => p.EighthExpertMarkValued).Where(x => x > 0);
            losesFromRisks[0].EighthExpertMarkValued = (eighthValued.Sum() / eighthValued.Count() / losesFromRisks[0].EighthExpertMark).Round();

            var ninthValued = losesFromRisks.Skip(1).Select(p => p.NinthExpertMarkValued).Where(x => x > 0);
            losesFromRisks[0].NinthExpertMarkValued = (ninthValued.Sum() / ninthValued.Count() / losesFromRisks[0].NinthExpertMark).Round();

            var tenthValued = losesFromRisks.Skip(1).Select(p => p.TenthExpertMarkValued).Where(x => x > 0);
            losesFromRisks[0].TenthExpertMarkValued = (tenthValued.Sum() / tenthValued.Count() / losesFromRisks[0].TenthExpertMark).Round();
            #endregion

            // переобчислення колонки додаткової вартості - сума по рядку поділена на суму оцінок експертів і помножена на вартість
            var expertMarksSum = losesFromRisks[0].SumOrAverage;
            for (int i = 1; i < rowsCount; i++)
            {
                var value = (GetValuedMarksSum(losesFromRisks, i) / expertMarksSum * losesFromRisks[i].Price).Round();
                losesFromRisks[i].AdditionalPrice = CheckResult(i, value);
            }

            // 1 рядок в цій колонці - сума елементів колонки
            losesFromRisks[0].AdditionalPrice = losesFromRisks.Skip(1).Select(x => x.AdditionalPrice).Where(x => x > 0).Sum().Round();

            // переобчислення конолки кінцевої вартості
            for(int i = 0; i < rowsCount; i++)
            {
                losesFromRisks[i].FinalPrice = (losesFromRisks[i].Price + losesFromRisks[i].AdditionalPrice).Round();
            }

            // копіювання нових значень у інші залежні колекції
            SetFinalPrice(projectRealisationPrices, priceIndex, losesFromRisks[0].FinalPrice);

            CopyAdditionalPrices(losesFromRisks, monitoringOfRisks, copyStart, copyEnd);

            //RecalculateMonitoringData(monitoringOfRisks, out intervals);
        }

        // Моніторинг
        public static void RecalculateMonitoringData(Collection<MonitoringOfRisk> monitoringOfRisks, List<(double Start, double End)> intervals)
        {
            static string SetUpPriorityLevel(double price, (double Start, double End) firstInterval, (double Start, double End) secondInterval, (double Start, double End) thirdInterval)
            {
                if (price < firstInterval.Start)
                {
                    return "ДУЖЕ НИЗЬКИЙ";
                }
                else if (price >= firstInterval.Start && price < firstInterval.End)
                {
                    return "НИЗЬКИЙ";
                }

                else if (price >= secondInterval.Start && price < secondInterval.End)
                {
                    return "СЕРЕДНІЙ";
                }
                else if (price >= thirdInterval.Start && price <= thirdInterval.End)
                {
                    return "ВИСОКИЙ";
                }
                else
                {
                    return "";
                }
            }

            static (double Max, double Min) FindMaxMin(IEnumerable<double> prices)
            {

                var maxPrice = prices.Max();
                var minPrice = prices.Min();

                return (maxPrice, minPrice);
            }

            const int TechRisksTotal = 11, PriceRisksTotal = 7,
             PlannedRisksTotal = 9, RealisationRisksTotal = 14,
             TechPos = 0, PricePos = 12, PlannedPos = 20, RealisationPos = 30;

            var tech = monitoringOfRisks.Skip(TechPos + 1).Take(TechRisksTotal).Select(x => x.Price);
            var price = monitoringOfRisks.Skip(PricePos + 1).Take(PriceRisksTotal).Select(x => x.Price);
            var planned = monitoringOfRisks.Skip(PlannedPos + 1).Take(PlannedRisksTotal).Select(x => x.Price);
            var realisation = monitoringOfRisks.Skip(RealisationPos + 1).Take(RealisationRisksTotal).Select(x => x.Price);

            var maxMins = new List<(double Max, double Min)>
            {
                // перенесення даних
                FindMaxMin(tech),
                FindMaxMin(price),
                FindMaxMin(planned),
                FindMaxMin(realisation),
            };

            var max = maxMins.Select(x => x.Max).Max();
            var min = maxMins.Select(x => x.Min).Min();

            var intervalLength = Math.Abs(max - min) / 3;

            // створення проміжків
            (double Start, double End) firstInterval = (min.Round(), (min + intervalLength).Round());
            (double Start, double End) secondInterval = (firstInterval.End, (firstInterval.End + intervalLength).Round());
            (double Start, double End) thirdInterval = (secondInterval.End, max.Round());

            intervals = new List<(double Start, double End)>() { firstInterval, secondInterval, thirdInterval };

            foreach (var m in monitoringOfRisks)
            {
                m.PriorityLevel = SetUpPriorityLevel(m.Price, firstInterval, secondInterval, thirdInterval);
            }
        }

        #region Helper methods
        private static double CalculatePercent(int num1, int num2)
        {
            return Math.Round((double)num1 / num2 * 100, 2);
        }

        private static double GetMarksSum(Collection<ProbabilityOfRiskEvent> probabilityOfRiskEvents, int rowIndex)
        {
            return probabilityOfRiskEvents[rowIndex].FirstExpertMark + probabilityOfRiskEvents[rowIndex].SecondExpertMark +
                probabilityOfRiskEvents[rowIndex].ThirdExpertMark + probabilityOfRiskEvents[rowIndex].FourthExpertMark +
                probabilityOfRiskEvents[rowIndex].FifthExpertMark + probabilityOfRiskEvents[rowIndex].SixthExpertMark +
                probabilityOfRiskEvents[rowIndex].SeventhExpertMark + probabilityOfRiskEvents[rowIndex].EighthExpertMark +
                probabilityOfRiskEvents[rowIndex].NinthExpertMark + probabilityOfRiskEvents[rowIndex].TenthExpertMark;
        }

        private static double GetValuedMarksSum(Collection<ProbabilityOfRiskEvent> probabilityOfRiskEvents, int rowIndex)
        {
            return probabilityOfRiskEvents[rowIndex].FirstExpertMarkValued + probabilityOfRiskEvents[rowIndex].SecondExpertMarkValued +
               probabilityOfRiskEvents[rowIndex].ThirdExpertMarkValued + probabilityOfRiskEvents[rowIndex].FourthExpertMarkValued +
               probabilityOfRiskEvents[rowIndex].FifthExpertMarkValued + probabilityOfRiskEvents[rowIndex].SixthExpertMarkValued +
               probabilityOfRiskEvents[rowIndex].SeventhExpertMarkValued + probabilityOfRiskEvents[rowIndex].EighthExpertMarkValued +
               probabilityOfRiskEvents[rowIndex].NinthExpertMarkValued + probabilityOfRiskEvents[rowIndex].TenthExpertMarkValued;
        }

        private static double GetMarksSum(Collection<LosesFromRisk> lossesFromRisks, int rowIndex)
        {
            return lossesFromRisks[rowIndex].FirstExpertMark + lossesFromRisks[rowIndex].SecondExpertMark +
                lossesFromRisks[rowIndex].ThirdExpertMark + lossesFromRisks[rowIndex].FourthExpertMark +
                lossesFromRisks[rowIndex].FifthExpertMark + lossesFromRisks[rowIndex].SixthExpertMark +
                lossesFromRisks[rowIndex].SeventhExpertMark + lossesFromRisks[rowIndex].EighthExpertMark +
                lossesFromRisks[rowIndex].NinthExpertMark + lossesFromRisks[rowIndex].TenthExpertMark;
        }

        private static double GetValuedMarksSum(Collection<LosesFromRisk> lossesFromRisks, int rowIndex)
        {
            return lossesFromRisks[rowIndex].FirstExpertMarkValued + lossesFromRisks[rowIndex].SecondExpertMarkValued +
               lossesFromRisks[rowIndex].ThirdExpertMarkValued + lossesFromRisks[rowIndex].FourthExpertMarkValued +
               lossesFromRisks[rowIndex].FifthExpertMarkValued + lossesFromRisks[rowIndex].SixthExpertMarkValued +
               lossesFromRisks[rowIndex].SeventhExpertMarkValued + lossesFromRisks[rowIndex].EighthExpertMarkValued +
               lossesFromRisks[rowIndex].NinthExpertMarkValued + lossesFromRisks[rowIndex].TenthExpertMarkValued;
        }

        private static double GetMarksAverage(Collection<ProbabilityOfRiskEvent> probabilityOfRiskEvents, int rowIndex)
        {
            return GetMarksSum( probabilityOfRiskEvents, rowIndex) / 10;
        }

        private static double GetMarksAverage(Collection<LosesFromRisk> lossesFromRisks, int rowIndex)
        {
            return GetMarksSum(lossesFromRisks, rowIndex) / 10;
        }

        private static double GetProbabilitiesAverage(Collection<ProbabilityOfRiskEvent> probabilityOfRiskEvents)
        {
            var probabilities = probabilityOfRiskEvents.Skip(1).Select(x => x.Probability).Where(x => x > 0);
            return probabilities.Sum() / probabilities.Count();
        }

        private static string EvaluateRiskLevel(double value)
        {
            return value switch
            {
                < 0.1 => "ДУЖЕ НИЗЬКОЮ",
                >= 0.1 and < 0.25 => "НИЗЬКОЮ",
                >= 0.25 and < 0.5 => "СЕРЕДНЬОЮ",
                >= 0.5 and < 0.75 => "ВИСОКОЮ",
                _ => "ДУЖЕ ВИСОКОЮ",
            };
        }

        private static void CopyCount(Collection<SourceOfRisk> from, Collection<ProbabilityOfRiskEvent> to1, Collection<LosesFromRisk> to2, int start, int end)
        {
            for (int i = start, k = 0; i < end; i++, k++)
            {
                to1[k].Count = from[i].Count;
                to2[k].Count = from[i].Count;
            }
        }

        private static void CopyCount(Collection<SourceOfRisk> from, Collection<MonitoringOfRisk> to)
        {
            for (int i = 0; i < from.Count - 1; i++)
            {
                to[i].Count = from[i].Count;
            }
        }

        private static void SetFinalPrice(Collection<ProjectRealisationPrice> projectRealisationPrices, int priceIndex, double value)
        {
            var finalPrices = projectRealisationPrices[1];
            switch(priceIndex)
            {
                case 1:
                    finalPrices.Price1 = value;
                    break;
                case 2:
                    finalPrices.Price2 = value;
                    break;
                case 3:
                    finalPrices.Price3 = value;
                    break;
                case 4:
                    finalPrices.Price4 = value;
                    break;
            }
        }

        private static void CopyAdditionalPrices(Collection<LosesFromRisk> from, Collection<MonitoringOfRisk> to, int start, int end)
        {
            for (int i = start, k = 0; i < end; i++, k++)
            {
                to[i].Price = from[k].AdditionalPrice;

            }
        }
        #endregion
    }
}
