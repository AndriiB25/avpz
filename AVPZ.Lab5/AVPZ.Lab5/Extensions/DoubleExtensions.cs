﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AVPZ.Lab5.Extensions
{
    public static class DoubleExtensions
    {
        public static double Round(this double value)
        {
            return Math.Round(value, 2);
        }
    }
}
