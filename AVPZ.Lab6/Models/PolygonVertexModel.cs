﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace RAPZ_Lab6.Models
{
    public class PolygonVertexModel
    {
        private List<double> expertsMarks;
        private List<double> expertsWeights;
        int size;
        double sumWeight;
        double koefWeight;

        private List<double> segments;
        private List<double> segmentsLines;
        private List<double> degrees;
        private List<double> radians;
        private List<double> koeficients;
        private List<double> aList;
        private List<double> bList;
        private List<double> cList;

        private List<double> squares;
        double sumSquare;

        private PointCollection points;


        public PolygonVertexModel(List<double> expertsMarks, List<double> expertsWeights, double koef)
        {
            this.expertsMarks = expertsMarks;
            this.expertsWeights = expertsWeights;
            size = expertsMarks.Count;

            koefWeight = koef;

            foreach (var weight in expertsWeights)
            {
                sumWeight += weight;
            }

            segments = new List<double>();
            segmentsLines = new List<double>();
            degrees = new List<double>();
            radians = new List<double>();
            koeficients = new List<double>();
            aList = new List<double>();
            bList = new List<double>();
            cList = new List<double>();

            squares = new List<double>();
            sumSquare = 0;

            points = new PointCollection();

            CalculateData();
        }

        private void CalculateData()
        {

            for (int i = 0; i < size; i++)
            {
                //segments (частка круга)
                segments.Add((double)expertsWeights[i] / sumWeight * 360);


                if (i == 0)
                {
                    //segments lines
                    segmentsLines.Add(segments[0] / 2.0);
                    //degrees
                    degrees.Add(0);
                    //radians
                    radians.Add(0);

                }
                else
                {
                    //segments lines
                    segmentsLines.Add(segments[i] + segmentsLines[i - 1]);
                    //degrees
                    degrees.Add((segmentsLines[i] + segmentsLines[i - 1]) / 2);
                    //radians
                    radians.Add(degrees[i] * Math.PI / 180);
                }
            }

            for (int i = 0; i < size; i++)
            {
                //koeficients
                koeficients.Add(koefWeight * expertsWeights[i] * expertsMarks[i]);
                //a sides
                aList.Add(koeficients[i] * Math.Sin(radians[i]));
                //b sides
                bList.Add(koeficients[i] * Math.Cos(radians[i]));
                //c sides
                cList.Add(Math.Sqrt(Math.Pow(aList[i], 2) + Math.Pow(bList[i], 2)));
            }


            //calculate squares
            aList.Add(aList[0]);
            bList.Add(bList[0]);
            for (int i = 1; i < size + 1; i++)
            {
                squares.Add(Math.Abs(aList[i - 1] * bList[i] - bList[i - 1] * aList[i]));
                sumSquare += squares[i - 1];
            }
        }

        public double GetSumSquare()
        {
            return Math.Round(sumSquare, 2);
        }

        public PointCollection GetPoints()
        {
            for (int i = 0; i < size; i++)
            {
                points.Add(new Point(bList[i], -aList[i]));
            }
            return points;
        }
    }
}
