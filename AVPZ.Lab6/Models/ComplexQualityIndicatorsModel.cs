﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RAPZ_Lab6.Models
{
    public class ComplexQualityIndicatorsModel
    {
        public string? Critery { get; set; }
        public List<double> Evaluations { get; set; } = new();
        public List<double> AvarageEvaluations { get; set; } = new();
    }
}
