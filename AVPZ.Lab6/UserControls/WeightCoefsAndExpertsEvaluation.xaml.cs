﻿using RAPZ_Lab6.ViewModels;
using System.Windows.Controls;
namespace RAPZ_Lab6.UserControls
{
    /// <summary>
    /// Interaction logic for WeightCoefsAndExpertsEvaluation.xaml
    /// </summary>
    public partial class WeightCoefsAndExpertsEvaluation : UserControl
    {
        public static WeightCoefFromExpertViewModel WeightCoefFromExpertViewModel { get; set; } = new();

        public WeightCoefsAndExpertsEvaluation()
        {
            DataContext = WeightCoefFromExpertViewModel;

            InitializeComponent();
        }
    }
}
