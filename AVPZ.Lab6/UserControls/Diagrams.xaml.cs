﻿using RAPZ_Lab6.Models;
using RAPZ_Lab6.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.NetworkInformation;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using LiveCharts;
using LiveCharts.Wpf;
namespace RAPZ_Lab6.UserControls
{
    /// <summary>
    /// Interaction logic for Diagrams.xaml
    /// </summary>
    /// 


    public partial class Diagrams : UserControl
    {
        private void HideAll()
        {
            PolyAllDiagrams.Visibility = Visibility.Collapsed;
            PolyCanvas.Visibility = Visibility.Collapsed;
            PolyUsability.Visibility = Visibility.Collapsed;
            PolyUsers.Visibility = Visibility.Collapsed;
            PolyProggraming.Visibility = Visibility.Collapsed;

            SquareLabel.Content = "Площа:";
        }
        private void ShowAll()
        {
            PolyAllDiagrams.Visibility = Visibility.Visible;
            PolyCanvas.Visibility = Visibility.Visible;
            PolyUsability.Visibility = Visibility.Visible;
            PolyUsers.Visibility = Visibility.Visible;
            PolyProggraming.Visibility = Visibility.Visible;
            SquareLabel.Content = "Площа:";
        }
        public Diagrams()
        {
            InitializeComponent();
            var ellipse = EllipseBG;
            ellipse.Height = 2*100;
            ellipse.Width = 2*100;
            ellipse.Fill = Brushes.Blue;
            ellipse.StrokeThickness = 2;
            ellipse.Stroke = Brushes.Black;

            Canvas.SetTop(ellipse, 50);
            Canvas.SetLeft(ellipse, 50);

            Canvas.SetTop(PolyCanvas, 150);
            Canvas.SetLeft(PolyCanvas, 150);
            PolyCanvas.StrokeThickness = 2;
            PolyCanvas.Stroke = Brushes.Black;
            PolyCanvas.Fill = Brushes.Yellow;

            Canvas.SetTop(PolyUsability, 150);
            Canvas.SetLeft(PolyUsability, 150);
            PolyUsability.StrokeThickness = 2;
            PolyUsability.Stroke = Brushes.Black;
            PolyUsability.Fill = Brushes.Yellow;

            Canvas.SetTop(PolyUsers, 150);
            Canvas.SetLeft(PolyUsers, 150);
            PolyUsers.StrokeThickness = 2;
            PolyUsers.Stroke = Brushes.Black;
            PolyUsers.Fill = Brushes.Yellow;

            Canvas.SetTop(PolyProggraming, 150);
            Canvas.SetLeft(PolyProggraming, 150);
            PolyProggraming.StrokeThickness = 2;
            PolyProggraming.Stroke = Brushes.Black;
            PolyProggraming.Fill = Brushes.Yellow;
        }

        private void InsustryExperts_Checked(object sender, RoutedEventArgs e)
        {

            HideAll();
            PolyCanvas.Visibility = Visibility.Visible;

            Polygon poly = PolyCanvas;
            poly.StrokeThickness = 2;
            poly.Stroke = Brushes.Black;
            PointCollection points = new PointCollection();

            #region metricks
            List<double> weights = GlobalViewModel.WeightCoefFromExpert.WeightCoefFromExpert.Select(w => w.Weights[0]).ToList();
            List<double> marks = GlobalViewModel.WeightCoefFromExpert.WeightCoefFromExpert.Select(e => e.Evaluations[0]).ToList();
            #endregion

            var polyVertex = new PolygonVertexModel(marks, weights, 0.7);
            poly.Points = polyVertex.GetPoints();
     
            double square = polyVertex.GetSumSquare();
            SquareLabel.Content = "Площа: " + square;
        }

        private void UsabilityExperts_Checked(object sender, RoutedEventArgs e)
        {
            HideAll();
            PolyUsability.Visibility= Visibility.Visible;

            Polygon poly = PolyUsability;
            poly.StrokeThickness = 2;
            poly.Stroke = Brushes.Black;
            PointCollection points = new PointCollection();

            #region metricks
            List<double> weights = GlobalViewModel.WeightCoefFromExpert.WeightCoefFromExpert.Select(w => w.Weights[1]).ToList();
            List<double> marks = GlobalViewModel.WeightCoefFromExpert.WeightCoefFromExpert.Select(e => e.Evaluations[1]).ToList();
            #endregion

            var polyVertex = new PolygonVertexModel(marks, weights, 0.7);
            poly.Points = polyVertex.GetPoints();

            double square = polyVertex.GetSumSquare();
            SquareLabel.Content = "Площа: " + square;
        }

        private void ProggramingExperts_Checked(object sender, RoutedEventArgs e)
        {
            HideAll();
            PolyProggraming.Visibility = Visibility.Visible;

            Polygon poly = PolyProggraming;
            poly.StrokeThickness = 2;
            poly.Stroke = Brushes.Black;
            PointCollection points = new PointCollection();

            #region metrics
            List<double> weights = GlobalViewModel.WeightCoefFromExpert.WeightCoefFromExpert.Select(w => w.Weights[2]).ToList();
            List<double> marks = GlobalViewModel.WeightCoefFromExpert.WeightCoefFromExpert.Select(e => e.Evaluations[2]).ToList();
            #endregion

            var polyVertex = new PolygonVertexModel(marks, weights, 0.7);
            poly.Points = polyVertex.GetPoints();

            double square = polyVertex.GetSumSquare();

            SquareLabel.Content = "Площа: " + square;
        }

        private void Users_Checked(object sender, RoutedEventArgs e)
        {
            HideAll();
            PolyUsers.Visibility = Visibility.Visible;

            Polygon poly = PolyUsers;
            poly.StrokeThickness = 2;
            poly.Stroke = Brushes.Black;
            PointCollection points = new PointCollection();

            #region metricks
            List<double> weights = GlobalViewModel.WeightCoefFromExpert.WeightCoefFromExpert.Select(w => w.Weights[3]).ToList();
            List<double> marks = GlobalViewModel.WeightCoefFromExpert.WeightCoefFromExpert.Select(e => e.Evaluations[3]).ToList();
            #endregion

            var polyVertex = new PolygonVertexModel(marks, weights, 0.7);
            poly.Points = polyVertex.GetPoints();

            double square = polyVertex.GetSumSquare();
            SquareLabel.Content = "Площа: " + square;
        }

        private void AllDiagrams_Checked(object sender, RoutedEventArgs e)
        {
            ShowAll();

            SquareLabel.Content = "";
        }
    }
}
