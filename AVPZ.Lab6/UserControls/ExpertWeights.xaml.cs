﻿using RAPZ_Lab6.Models;
using RAPZ_Lab6.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace RAPZ_Lab6.UserControls
{
    /// <summary>
    /// Interaction logic for ExpertWeights.xaml
    /// </summary>
    public partial class ExpertWeights : UserControl
    {
        public static List<ExpertWeightModel>? ExpertsWeights { get; set; } = new();

        private int _expertsCount = 4;

        public ExpertWeights()
        {
            #region ExpertsWeightss
            Random random = new(Guid.NewGuid().GetHashCode());

            ExpertsWeights.Add(new ExpertWeightModel() { ExpertType = "Експерт галузі" });
            ExpertsWeights.Add(new ExpertWeightModel() { ExpertType = "Експерт юзабіліті" });
            ExpertsWeights.Add(new ExpertWeightModel() { ExpertType = "Експерт з програмування" });
            ExpertsWeights.Add(new ExpertWeightModel() { ExpertType = "Потенційні користувачі" });

            for (int i = 0; i < _expertsCount; i++)
            {
                double value = (double)random.Next(5, 9) / 10.0;
                ExpertsWeights[i].CoefOfWeightRelational = value;
            }
            #endregion
            DataContext = ExpertsWeights;

            InitializeComponent();
        }
    }
}
